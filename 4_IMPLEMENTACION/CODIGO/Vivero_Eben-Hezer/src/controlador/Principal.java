package controlador;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.ResourceBundle;

















import sun.awt.ModalityEvent;
import vista.Main;
import modelo.Usuario;
import javafx.beans.property.ObjectProperty;
//import modelo.LoginVO;
//import application.Principal.btnSalir_click;
//import controlador.Principal.btnClientes_click;
//import controlador.Principal.btnUsuarios_click;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.effect.BlendMode;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Text;

public class Principal implements Initializable {
	
	private @FXML Button btnSalir;
	private @FXML Button btnProductos;
	private @FXML Button btnClientes;
	private @FXML Button btnProveedores, btnUsuarios;
	private @FXML Button btnVentas, btnVentas1, btnCompras1, btnCompras;
	private @FXML DatePicker dtpFecha;
	@FXML Text txBienvenida;
	@FXML private Label lblVender, lblComprar;
	private vista.Main main;
	private controlador.ConVentas venta;
	private Ventanas ventanas;
	@FXML private BorderPane bdpPrincipal;
	private ObjectProperty<BlendMode> modelo;
	
	private Usuario usuario;
	
	public static String nivel;
	public static String nombre;
	public static int codigo;
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		getFechaActual();
		txBienvenida.setText(" "+nombre);
		System.out.println(nivel);
		if(nivel.equals("Empleado")){
			btnVentas.setVisible(false);
			btnCompras.setVisible(false);
			btnClientes.setVisible(false);
			btnProductos.setVisible(false);
			btnProveedores.setVisible(false);
			btnUsuarios.setVisible(false);
			lblComprar.setVisible(false);
			lblVender.setVisible(false);
			btnCompras1.setVisible(true);
			btnVentas1.setVisible(true);
		}
		if(nivel.equals("Administrador")){
			btnVentas.setVisible(true);
			btnCompras.setVisible(true);
			btnClientes.setVisible(true);
			btnProductos.setVisible(true);
			btnProveedores.setVisible(true);
			btnUsuarios.setVisible(true);
			lblComprar.setVisible(true);
			lblVender.setVisible(true);
			btnCompras1.setVisible(false);
			btnVentas1.setVisible(false);
		}
	}
	
	public Principal(){
		ventanas = Ventanas.getInstancia();
	}
	
	@FXML public void clickClientes(){
		//bdpPrincipal.setBlendMode(BlendMode.EXCLUSION);
		ventanas.asignarAnchorPane("../vista/fxml/Clientes.fxml");
	}
	
	@FXML public void clickProductos(){
		//bdpPrincipal.setBlendMode(BlendMode.EXCLUSION);
		ventanas.asignarBorderCentro("../vista/fxml/Productos.fxml", "", 0);
	}
	
	@FXML public void click_Proveedores(){
		//bdpPrincipal.setBlendMode(BlendMode.EXCLUSION);
		ventanas.asignarAnchorPane("../vista/fxml/Proveedores.fxml");
	}
	
	@FXML public void click_Ventas(){
		//bdpPrincipal.setBlendMode(BlendMode.EXCLUSION);
		//ventanas.asignarAnchorPane("../vista/fxml/VentasAdmin.fxml");
	}
	@FXML public void entrar(){
		//bdpPrincipal.setBlendMode(BlendMode.EXCLUSION);
		if(usuario == null){
			usuario = new Usuario();
		}
		usuario.setNombre(nombre);
		usuario.setId_usuario(codigo);
			ventanas=Ventanas.getInstancia();
			ventanas.setPrimaryStage(Main.getPrimaryStage());
			usuario.getNombre();
			ventanas.asignarBorderCentro("../vista/fxml/Venta.fxml", usuario.getNombre(), usuario.getId_usuario());
	}
	
	public void compras(){
		//bdpPrincipal.setBlendMode(BlendMode.EXCLUSION);
		if(usuario == null){
			usuario = new Usuario();
		}
		usuario.setNombre(nombre);
		usuario.setId_usuario(codigo);
			ventanas=Ventanas.getInstancia();
			ventanas.setPrimaryStage(Main.getPrimaryStage());
		ventanas.asignarBorderSur("../vista/fxml/Compra.fxml", usuario.getNombre(), usuario.getId_usuario());
	}
	public void entrar2(){
		
	}
	@FXML public void vender(){
		//bdpPrincipal.setBlendMode(BlendMode.EXCLUSION);
		if(usuario == null){
			usuario = new Usuario();
		}
		usuario.setNombre(nombre);
		usuario.setId_usuario(codigo);
			ventanas=Ventanas.getInstancia();
			ventanas.setPrimaryStage(Main.getPrimaryStage());
			usuario.getNombre();
			ventanas.asignarBorderCentro("../vista/fxml/Venta.fxml", usuario.getNombre(), usuario.getId_usuario());
	}
	@FXML public void comprar(){
		//bdpPrincipal.setBlendMode(BlendMode.EXCLUSION);
		if(usuario == null){
			usuario = new Usuario();
		}
		usuario.setNombre(nombre);
		usuario.setId_usuario(codigo);
			ventanas=Ventanas.getInstancia();
			ventanas.setPrimaryStage(Main.getPrimaryStage());
		ventanas.asignarBorderSur("../vista/fxml/Compra.fxml", usuario.getNombre(), usuario.getId_usuario());
	}
	@FXML public void clickUsuarios(){
		//bdpPrincipal.setBlendMode(BlendMode.EXCLUSION);
		ventanas.asignarAnchorPane("../vista/fxml/Usuario.fxml");
	}
	public String getFechaActual() {
        Date ahora = new Date();
        SimpleDateFormat formateador = new SimpleDateFormat("dd-MM-yyyy");
        dtpFecha.setPromptText(formateador.format(ahora));
		return null;
	
    }

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public void disable(){
		btnClientes.setDisable(true);
		btnProductos.setDisable(true);
	}
	
	public void noDisable(){
		System.out.println("como es de que aqui no marca error");
	}

	@FXML public void salir(){
		
			System.exit(0);
		
	}

	public controlador.ConVentas getVenta() {
		return venta;
	}

	public void setVenta(controlador.ConVentas venta) {
		this.venta = venta;
	}

}
