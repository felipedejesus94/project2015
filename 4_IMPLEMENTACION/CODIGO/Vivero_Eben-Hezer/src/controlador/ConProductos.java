package controlador;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import sun.awt.SunHints.Value;
import vista.Main;
import modelo.Categoria;
import modelo.Clientes;
import modelo.Productos;
import modelo.UnidadMedida;
import javafx.beans.InvalidationListener;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Pagination;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Text;

public class ConProductos implements Initializable{
	@FXML private Button btnGuardar, btnModificar, btnEliminar, btnAddCategoria, btnAddUnidad, btnActualizar;
	@FXML private TextField txtCodigo, txtNombre, txtCientifico, txtPrecio2, txtPrecio1, txtTamanio, txtMaximo, txtMinimo, txtCantidad, txtBuscador;
	@FXML private Label lblMensaje, lblCategoria, lblLimpiar, lblRecuperar, lblInactivos, lblCodigo;
	@FXML private Text lblProductos;
	@FXML private ComboBox<UnidadMedida> cbUnidad;
	@FXML private ComboBox<Categoria> cbCategoria;
	@FXML private TableColumn nombreColumn, cientificoColumn, precio2Column, precio1Column, cantidadColumn;
	@FXML private TableView<Productos> tblProductos;
	@FXML private Pagination paginador;
	@FXML private ImageView imgBasura;
	@FXML private CheckBox ckbInactivo;
	private Categoria c;
	private UnidadMedida u;
	
	private FilteredList<Productos> datosBusqueda;
	private ObservableList<Productos> datos;
	private Productos p;
	private int filasXPagina;
	private Errores ce;
	private Ventanas ventanas;
	
	public ConProductos(){
		ventanas = Ventanas.getInstancia();
		p = new Productos();
		filasXPagina = 10;
		datos = FXCollections.observableArrayList();
		c = new Categoria();
		u = new UnidadMedida();
		ce = new Errores();
	}
	
	@FXML public void categoria(){
		ventanas = Ventanas.getInstancia();
		ventanas.setPrimaryStage(Main.getPrimaryStage());
		ventanas.categoria("../vista/fxml/Categoria.fxml", "Registro Categoria");
	}
	
	@FXML public void unidadMedida(){
		ventanas = Ventanas.getInstancia();
		ventanas.setPrimaryStage(Main.getPrimaryStage());
		ventanas.unidad_medida("../vista/fxml/Unidad.fxml", "Registro Unidad de Medida");
	}
	
	@FXML public void ingresarProducto(){
		try{
			if(txtNombre.getText().trim().isEmpty()|txtCientifico.getText().trim().isEmpty()| txtPrecio2.getText().trim().isEmpty()| txtPrecio1.getText().
				isEmpty()|txtTamanio.getText().trim().isEmpty()|cbUnidad.getSelectionModel().getSelectedItem() == null | txtMaximo.getText().trim().
				isEmpty()|txtMinimo.getText().trim().isEmpty()|txtCantidad.getText().trim().isEmpty()|cbCategoria.getSelectionModel().getSelectedItem() == null){
				lblMensaje.setText("faltan datos por ingresar");
			}else{
				String num = txtPrecio1.getText();
				Pattern pa = Pattern.compile("[A-Za-z.@_-~#]");
				Matcher ma = pa.matcher(num);
				if(!ma.find()){
					String nume = txtPrecio2.getText();
					Pattern pe = Pattern.compile("[A-Za-z.@_-~#]");
					Matcher me = pe.matcher(nume);
					if(!me.find()){
						int precio1 = Integer.parseInt(txtPrecio1.getText());
						int precio2 = Integer.parseInt(txtPrecio2.getText());
						if(precio1 <= 0 | precio1 > 2000 | precio2 <= 0 | precio2 > 2000){
							lblMensaje.setText("Verifique los campos de precio");
						}else{
							String tama = txtTamanio.getText();
							Pattern pat = Pattern.compile("[A-Za-z.@_-~#]");
							Matcher mtc = pat.matcher(tama);
							if(!mtc.find()){
								int tamano = Integer.parseInt(txtTamanio.getText());
								if(tamano<=0 | tamano>100){
									lblMensaje.setText("Verifique el campo tama�o");
								}else{
									String max = txtMaximo.getText();
									Pattern mp = Pattern.compile("[A-Za-z.@_-~#]");
									Matcher mx = mp.matcher(max);
									if(!mx.find()){
										String mex = txtMinimo.getText();
										Pattern mpe = Pattern.compile("[A-Za-z.@_-~#]");
										Matcher mxe = mpe.matcher(mex);
										if(!mxe.find()){
											String de = txtCantidad.getText();
											Pattern zo = Pattern.compile("[A-Za-z.@_-~#]");
											Matcher te = zo.matcher(de);
											if(!te.find()){
											int maximo = Integer.parseInt(txtMaximo.getText());
											int minimo = Integer.parseInt(txtMinimo.getText());
											int cantidad = Integer.parseInt(txtCantidad.getText());
											if(maximo<=0 | maximo>2000 | minimo <= 0 | minimo > 1000 | cantidad <= 0 | cantidad > 2000){
												lblMensaje.setText("Verifique las cantidades introducidas");
											}else{
												p = new Productos();
												p.setId_producto(new SimpleIntegerProperty(Integer.valueOf(txtCodigo.getText())));
												p.setNombre(new SimpleStringProperty(txtNombre.getText()));
												p.setCientifico(new SimpleStringProperty(txtCientifico.getText()));
												p.setPrecio2(new SimpleFloatProperty(Float.valueOf(txtPrecio2.getText())));
												p.setPrecio1(new SimpleFloatProperty(Float.valueOf(txtPrecio1.getText())));
												p.setTamanio(new SimpleDoubleProperty(Double.valueOf(txtTamanio.getText())));
												p.setNombre_unidad(cbUnidad.getSelectionModel().getSelectedItem());
												p.setMaximo(new SimpleIntegerProperty(Integer.valueOf(txtMaximo.getText())));
												p.setMinimo(new SimpleIntegerProperty(Integer.valueOf(txtMinimo.getText())));
												p.setExistentes(new SimpleIntegerProperty(Integer.valueOf(txtCantidad.getText())));
												p.setNombre_categoria(cbCategoria.getSelectionModel().getSelectedItem());
															
												boolean yes = p.insertar();
															
												if(yes){
													llenarTabla(true);
													lblMensaje.setText("se registro correctamente");
													limpiar();
												}else{
													lblMensaje.setText("no se pudo guardar");
												}
											}
											}else{
												lblMensaje.setText("Campo Existentes, s�lo �tiliza numeros");
											}
										}else{
											lblMensaje.setText("Campo Minimo, s�lo �tiliza numeros");
										}
									}else{
										lblMensaje.setText("Campos de m�ximo solo utilizan numeros");
									}
								}
							}else{
								lblMensaje.setText("El campo tama�o solo �tiliza numeros");
							}
						}
					}else{
						lblMensaje.setText("Introduzca solo numeros en los campos de precio");
					}
				}else{
					lblMensaje.setText("Introduzca solo numeros en los campos de precio");
				}
			}
		}catch(Exception e){
			ce.printLog(e.getMessage(), this.getClass().toString());
			e.printStackTrace();
		}
	}
	
	@FXML public void modificarProducto(){
		try{
			if(txtNombre.getText().trim().isEmpty()|txtCientifico.getText().trim().isEmpty()| txtPrecio2.getText().trim().isEmpty()| txtPrecio1.getText().
					isEmpty()|txtTamanio.getText().trim().isEmpty()|cbUnidad.getSelectionModel().getSelectedItem() == null | txtMaximo.getText().trim().
					isEmpty()|txtMinimo.getText().trim().isEmpty()|txtCantidad.getText().trim().isEmpty()|cbCategoria.getSelectionModel().getSelectedItem() == null){
				lblMensaje.setText("faltan datos por ingresar");
			}else{
				p = new Productos();
				p.setId_producto(new SimpleIntegerProperty(Integer.valueOf(txtCodigo.getText())));
				p.setNombre(new SimpleStringProperty(txtNombre.getText()));
				p.setCientifico(new SimpleStringProperty(txtCientifico.getText()));
				p.setPrecio2(new SimpleFloatProperty(Float.valueOf(txtPrecio2.getText())));
				p.setPrecio1(new SimpleFloatProperty(Float.valueOf(txtPrecio1.getText())));
				p.setTamanio(new SimpleDoubleProperty(Double.valueOf(txtTamanio.getText())));
				p.setNombre_unidad(cbUnidad.getSelectionModel().getSelectedItem());
				p.setMaximo(new SimpleIntegerProperty(Integer.valueOf(txtMaximo.getText())));
				p.setMinimo(new SimpleIntegerProperty(Integer.valueOf(txtMinimo.getText())));
				p.setExistentes(new SimpleIntegerProperty(Integer.valueOf(txtCantidad.getText())));
				p.setNombre_categoria(cbCategoria.getSelectionModel().getSelectedItem());
				
				boolean yes = p.modificar();
				if(yes){
					llenarTabla(true);
					lblMensaje.setText("se ha modificado correctamente");
					limpiar();
				}else{
					lblMensaje.setText("no fue posible modificar");
				}
			}
		}catch(Exception e){
			ce.printLog(e.getMessage(), this.getClass().toString());
		}
	}
	
	@FXML public void eliminarProducto() throws SQLException{
		if(txtCodigo.getText().trim().isEmpty()){
			lblMensaje.setText("Debe seleccionar un regstro");
		}else{
			p = new Productos();
			p.setId_producto(new SimpleIntegerProperty(Integer.valueOf(txtCodigo.getText())));
			if(p.eliminar() == true){
				this.llenarTabla(true);
				limpiar();
				lblMensaje.setText("Se ha eliminado el producto");
			}else{
				lblMensaje.setText("Se ha producido un error en el servidor");
			}
		}
	}
	
	public void limpiar(){
		txtCodigo.setDisable(true);
		txtCantidad.clear();
		txtCientifico.clear();
		txtCodigo.clear();
		txtMaximo.clear();
		txtMinimo.clear();
		txtNombre.clear();
		txtPrecio1.clear();
		txtPrecio2.clear();
		txtTamanio.clear();
		cbCategoria.getSelectionModel().clearSelection();
		cbUnidad.getSelectionModel().select(-1);
	}
	public void llenarTabla(boolean estatus) throws SQLException{
		try{
			nombreColumn.setCellValueFactory(new PropertyValueFactory<Productos, String>("nombre"));
			cientificoColumn.setCellValueFactory(new PropertyValueFactory<Productos, String>("cientifico"));
			precio1Column.setCellValueFactory(new PropertyValueFactory<Productos, String>("precio1"));
			precio2Column.setCellValueFactory(new PropertyValueFactory<Productos, String>("precio2"));
			cantidadColumn.setCellValueFactory(new PropertyValueFactory<Productos, String>("existentes"));
			
			datos = p.getProductos(estatus);
			datosBusqueda = new FilteredList<>(datos);
			paginador.setPageCount(datosBusqueda.size()/filasXPagina);
			paginador.setPageFactory((Integer pagina) -> createPage(pagina));
			lblMensaje.setText("Total: "+datos.size()+ " registros");
		}catch(Exception e){
			ce.printLog(e.getMessage(), this.getClass().toString());
		}
	}

	private Node createPage(int pageIndex){
		int fromIndex = pageIndex * filasXPagina;
		int toIndex = Math.min(fromIndex + filasXPagina, datosBusqueda.size());
		tblProductos.setItems(FXCollections.observableArrayList(datosBusqueda.subList(fromIndex, toIndex)));
		return new BorderPane(tblProductos);
	}
	
	@FXML public void buscarTexto() throws SQLException{
		if(txtBuscador.getText().trim().isEmpty()){
			datosBusqueda = new FilteredList<> (datos);
			filasXPagina = 10;
			paginador.setPageCount(datosBusqueda.size()/filasXPagina);
			paginador.setPageFactory((Integer pagina) -> createPage(pagina));
			lblMensaje.setText(datosBusqueda.size()+" Registros en la base de datos");
		}else{
			try{
				datosBusqueda.setPredicate(film -> film.getNombre().toLowerCase().contains(txtBuscador.getText().toLowerCase()));
				if(datosBusqueda.size()< 10)
					filasXPagina = datosBusqueda.size();
				else
					filasXPagina = 10;
				paginador.setPageCount(datosBusqueda.size()/filasXPagina);
				paginador.setPageFactory((Integer pagina)-> createPage(pagina));
				lblMensaje.setText(datosBusqueda.size() + " Resultados encontrados");
			}catch(Exception e){
				llenarTabla(true);
				lblMensaje.setText("No se encontraron resultados");
				filasXPagina = 0;
				paginador.setPageCount(filasXPagina);
				paginador.setPageFactory((Integer pagina) -> createPage(pagina));
				ce.printLog(e.getMessage(), this.getClass().toString());
			}
		}
	}
	
	@FXML public void click_tabla(){
		if(tblProductos.getSelectionModel().getSelectedItem() != null){
			txtCodigo.setDisable(false);
			p = tblProductos.getSelectionModel().getSelectedItem();
			txtCodigo.setText(p.getId_producto()+"");
			txtCantidad.setText(p.getExistentes()+"");
			txtCientifico.setText(p.getCientifico());
			txtMinimo.setText(p.getMinimo()+"");
			txtMaximo.setText(p.getMaximo()+"");
			txtNombre.setText(p.getNombre());
			txtPrecio1.setText(p.getPrecio1()+"");
			txtPrecio2.setText(p.getPrecio2()+"");
			txtTamanio.setText(p.getTamanio()+"");
			cbCategoria.getSelectionModel().select(p.getNombre_categoria());
			cbUnidad.getSelectionModel().select(p.getNombre_unidad());
		}
	}
	@FXML public void actualizar() throws SQLException{
		cbUnidad.setItems(u.getUnidad(true));
		cbCategoria.setItems(c.getCategoria(true));
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		txtCodigo.setDisable(true);
		try {
			cbUnidad.setItems(u.getUnidad(true));
			cbCategoria.setItems(c.getCategoria(true));
			llenarTabla(true);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			ce.printLog(e.getMessage(), this.getClass().toString());
		}
		imgBasura.setVisible(false);
		lblRecuperar.setVisible(false);
		lblInactivos.setVisible(false);
		
	}
	
	@FXML public void recuperar() throws SQLException{
		if(txtCodigo.getText().trim().isEmpty()){
			lblMensaje.setText("Seleccione un registro");
		}else{
			p = new Productos();
			p.setId_producto(new SimpleIntegerProperty(Integer.valueOf(txtCodigo.getText())));
			if(p.recuperarProducto() == true){
				lblMensaje.setText("Se recupero el producto");
				llenarTabla(false);
				limpiar();
				
			}
		}
	}
	@FXML public void click_inactivo() throws SQLException{
		if(ckbInactivo.isSelected()){
			llenarTabla(false);
			limpiar();
			lblInactivos.setVisible(true);
			lblProductos.setVisible(false);
			lblRecuperar.setVisible(true);
			imgBasura.setVisible(true);
			btnGuardar.setVisible(false);
			btnModificar.setVisible(false);
			btnEliminar.setVisible(false);
		}else{
			llenarTabla(true);
			limpiar();
			lblInactivos.setVisible(false);
			lblProductos.setVisible(true);
			lblRecuperar.setVisible(false);
			imgBasura.setVisible(false);
			btnGuardar.setVisible(true);
			btnModificar.setVisible(true);
			btnEliminar.setVisible(true);
		}
	}

}
