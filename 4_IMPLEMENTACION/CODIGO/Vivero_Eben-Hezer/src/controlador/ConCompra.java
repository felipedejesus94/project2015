package controlador;

import java.net.URL;
import java.sql.Date;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JOptionPane;

import javafx.beans.property.SimpleFloatProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;
import modelo.Clientes;
import modelo.Compra;
import modelo.Productos;
import modelo.Proveedores;
import modelo.Venta;

public class ConCompra implements Initializable{
	
	@FXML private TextField txtBuscador, txtProveedor, txtEmpleado, txtProducto, txtExistentes,
	txtPrecio, txtCantidad, txtCodEmpleado, txtCodProveedor, txtPrecio1, txtPrecio2, txtMaximo,
	txtFecha;
	@FXML private Button btnGuardar, btnCancelar, btnTicket, btnBuscar, btnEliminar, btnModificar,
	btnAgregar, btnNueva;
	@FXML private Text txTotal;
	@FXML private Label lblFolio;
	@FXML private ListView<Productos> lvProductos;
	@FXML private ListView<Proveedores> lvProveedores;
	@FXML private TableView<Productos> tvDetalles;
	@FXML private TableColumn<Productos, String> tcProducto, tcPrecio, tcCantidad, tcSubtotal;
	
	private ObservableList<Productos> listaDetalle;
	private ObservableList<Productos> elementos;
	private ObservableList<Proveedores> proveedores;
	private FilteredList<Productos> productosBusqueda;
	private FilteredList<Proveedores> proveBusqueda;
	private Productos p;
	private Proveedores pv;
	static String nombre;
	public static Integer codigo;
	private Compra cpa;
	private Principal prin;
	private int posicionPersonaEnLaTabla;
	private Errores ce;
	
	public ConCompra(){
		elementos = FXCollections.observableArrayList();
		p = new Productos();
		pv = new Proveedores();
		cpa = new Compra();
		prin = new Principal();
		ce = new Errores();
	}
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		try{
			txtCodEmpleado.setVisible(false);
			txtCodProveedor.setVisible(false);
			btnAgregar.setDisable(true);
			btnModificar.setDisable(true);
			btnEliminar.setDisable(true);
			folioVenta();
			txtEmpleado.setText(nombre);
			txtCodEmpleado.setText(codigo+"");
			elementos = p.getProductos(true);
			lvProductos.setItems(elementos);
			proveedores = pv.getProveedor(true);
			lvProveedores.setItems(proveedores);
			lvProveedores.setVisible(false);
			llenarTabla();
		}catch(Exception e){
			ce.printLog(e.getMessage(), this.getClass().toString());
		}
	}
	
	public void llenarTabla(){
		tcProducto.setCellValueFactory(new PropertyValueFactory<Productos, String>("nombre"));
		tcPrecio.setCellValueFactory(new PropertyValueFactory<Productos, String>("costo"));
		tcCantidad.setCellValueFactory(new PropertyValueFactory<Productos, String>("cantidad"));
		tcSubtotal.setCellValueFactory(new PropertyValueFactory<Productos, String>("subtotal"));
		listaDetalle = FXCollections.observableArrayList();
		tvDetalles.setItems(listaDetalle);
		
		final ObservableList<Productos> tablaPersonaSeleccionada = tvDetalles.getSelectionModel().getSelectedItems();
		tablaPersonaSeleccionada.addListener(selectorTablaPersonas);
	}
	private final ListChangeListener<Productos> selectorTablaPersonas =
            new ListChangeListener<Productos>() {
                @Override
                public void onChanged(ListChangeListener.Change<? extends Productos> c) {
                    ponerPersonaSeleccionada();
                }
	};
	private void ponerPersonaSeleccionada(){
		final Productos det = getTablaPersonaSeleccionada();
		posicionPersonaEnLaTabla = listaDetalle.indexOf(det);
		if(det != null){
			//det = tvDetalles.getSelectionModel().getSelectedItem();
			txtProducto.setText(det.getNombre());
			txtExistentes.setText(det.getExistentes()+"");
			txtCantidad.setText(det.getCantidad()+"");
		}
	}
	public Productos getTablaPersonaSeleccionada(){
		if(tvDetalles != null){
			List<Productos> tabla = tvDetalles.getSelectionModel().getSelectedItems();
			if(tabla.size()==1){
				final Productos camposeleccionado = tabla.get(0);
				return camposeleccionado;
			}
		}
		return null;
	}
	public void folioVenta(){
		if(cpa.Existe(true)){
			lblFolio.setText(cpa.getCompra_id()+"");
		}
	}
	
	public void ejecutarCompra(){
		if(cpa.guardar()){
			JOptionPane.showMessageDialog(null, "Se ha registrado la compra","Informaci�n",JOptionPane.INFORMATION_MESSAGE);
			clean();
			debeIngresar();
		}
	}
	public void doble(){
		clean();
		debeIngresar();
	}
	public void debeIngresar(){
		if(txtProveedor.getText().trim().isEmpty()){
			btnAgregar.setDisable(true);
			btnEliminar.setDisable(true);
			btnModificar.setDisable(true);
		}else{
			btnAgregar.setDisable(false);
			btnEliminar.setDisable(false);
			btnModificar.setDisable(false);
		}
	}
	public void limpiar(){
		txtCantidad.clear();
		txtPrecio.clear();
		txtExistentes.clear();
		txtProducto.clear();
		txtMaximo.clear();
		txtPrecio1.clear();
		txtPrecio2.clear();
	}
	public void clean(){
		lblFolio.setText("");;
		txtProveedor.clear();
		txtCodProveedor.clear();
		limpiar();
		llenarTabla();
	}
	public void refrescar(){
		clean();
		folioVenta();
		debeIngresar();
	}
	public void buscarProducto(){
		try{
			if(! txtBuscador.getText().trim().isEmpty()){
				productosBusqueda = new FilteredList<Productos>(elementos);
				productosBusqueda.setPredicate(Film -> Film.getNombre().toLowerCase().contains(txtBuscador.getText().toLowerCase()));
				lvProductos.setItems(productosBusqueda);
			}else{
				lvProductos.setItems(elementos);
			}
		}catch(Exception e){
			ce.printLog(e.getMessage(), this.getClass().toString());
		}
	}
	public void seleccionar(){
		try{
			Productos p = lvProductos.getSelectionModel().getSelectedItem();
			if(p != null){
				txtProducto.setText(p.getNombre());
				txtExistentes.setText(p.getExistentes()+"");
				txtMaximo.setText(p.getMaximo()+"");
			}
			debeIngresar();
			
		}catch(Exception e){
			ce.printLog(e.getMessage(), this.getClass().toString());
		}
	}
	public void aparecer() throws SQLException{
		lvProveedores.setVisible(true);
		actualiz();
	}
	public void actualiz() throws SQLException{
		proveedores = pv.getProveedor(true);
		lvProveedores.setItems(proveedores);
	}
	public void selecc(){
		try{
			Proveedores c = lvProveedores.getSelectionModel().getSelectedItem();
			if(c != null){
				txtProveedor.setText(c.getNombre());
				txtCodProveedor.setText(c.getId_proveedor()+"");
				lvProveedores.setVisible(false);
			}
		}catch(Exception e){
			ce.printLog(e.getMessage(), this.getClass().toString());
		}
	}
	public void buscarProveedor(){
		try{
			if(! txtProveedor.getText().trim().isEmpty()){
				proveBusqueda = new FilteredList<Proveedores>(proveedores);
				proveBusqueda.setPredicate(Film -> Film.getNombre().toLowerCase().contains(txtProveedor.getText().toLowerCase()));
				lvProveedores.setItems(proveBusqueda);
			}else{
				lvProveedores.setItems(proveedores);
			}
		}catch(Exception e){
			ce.printLog(e.getMessage(), this.getClass().toString());
		}
	}
	public void eliminarProducto(){
		try{
			cpa.setProductos(tvDetalles.getSelectionModel().getSelectedItem());
			cpa.setPosicionProductoEnLaTabla(posicionPersonaEnLaTabla);
			if(cpa.eliminarDetalle() == true){
				actualizarDetalles();
				limpiar();
				prueba();
			}
		}catch(Exception e){
			ce.printLog(e.getMessage(), this.getClass().toString());
		}
	}
	public void editarProducto(){
		try{
			int cantidad = Integer.parseInt(txtCantidad.getText());
			int existencias = Integer.parseInt(txtExistentes.getText());
			int maximo = Integer.parseInt(txtMaximo.getText());
			int result = cantidad + existencias;
			if(cantidad > 0 && result < maximo){
				cpa.setProductos(tvDetalles.getSelectionModel().getSelectedItem());
				cpa.setListaDetalles(listaDetalle);
				cpa.setCantidad(Integer.valueOf(txtCantidad.getText()));
				cpa.setExistentes(Integer.valueOf(txtExistentes.getText()));
				cpa.setCosto(Float.valueOf(txtPrecio.getText()));
				cpa.setPrecio1(Float.valueOf(txtPrecio1.getText()));
				cpa.setPrecio2(Float.valueOf(txtPrecio2.getText()));
				cpa.setCompra_id(Integer.valueOf(lblFolio.getText()));
				cpa.setMaximo(Integer.valueOf(txtMaximo.getText()));
				cpa.setProveedor_id(Integer.valueOf(txtCodProveedor.getText()));
				cpa.setEmpleado_id(Integer.valueOf(txtCodEmpleado.getText()));
				cpa.setFecha(Date.valueOf(txtFecha.getText()));
				cpa.setPosicionProductoEnLaTabla(posicionPersonaEnLaTabla);
				
				if(cpa.editarDetalle() == true){
					actualizarDetalles();
					limpiar();
					prueba();
				}
			}else{
				JOptionPane.showMessageDialog(null, "No puede superar el Stock M�ximo de productos","Informaci�n",JOptionPane.INFORMATION_MESSAGE);
			}
		}catch(Exception e){
			ce.printLog(e.getMessage(), this.getClass().toString());
		}
	}
	public void prueba() throws SQLException{
		elementos = p.getProductos(true);
		lvProductos.setItems(elementos);
	}
	
	public void agregarProducto(){
		try{
			String num =  txtCantidad.getText();
			Pattern p = Pattern.compile("[A-Za-z.@_-~#]");
			Matcher m = p.matcher(num);
			if(!m.find()){
				String nume =  txtPrecio.getText();
				Pattern pe = Pattern.compile("[A-Za-z.@_-~#]");
				Matcher me = pe.matcher(nume);
				if(!me.find()){
					String n =  txtPrecio1.getText();
					Pattern pa = Pattern.compile("[A-Za-z.@_-~#]");
					Matcher ma = pa.matcher(n);
					if(!ma.find()){
						String str =  txtPrecio2.getText();
						Pattern ter = Pattern.compile("[A-Za-z.@_-~#]");
						Matcher che = ter.matcher(str);
						if(!che.find()){
							int cantidad = Integer.parseInt(txtCantidad.getText());
							int existencias = Integer.parseInt(txtExistentes.getText());
							int maximo = Integer.parseInt(txtMaximo.getText());
							int result = cantidad + existencias;
							if(cantidad > 0 && result<maximo){
								cpa.setProductos(lvProductos.getSelectionModel().getSelectedItem());
								cpa.setListaDetalles(listaDetalle);
								cpa.setCantidad(Integer.valueOf(txtCantidad.getText()));
								cpa.setExistentes(Integer.valueOf(txtExistentes.getText()));
								cpa.setCompra_id(Integer.valueOf(lblFolio.getText()));
								cpa.setProveedor_id(Integer.valueOf(txtCodProveedor.getText()));
								cpa.setEmpleado_id(Integer.valueOf(txtCodEmpleado.getText()));
								cpa.setCosto(Float.valueOf(txtPrecio.getText()));
								cpa.setPrecio1(Float.valueOf(txtPrecio1.getText()));
								cpa.setPrecio2(Float.valueOf(txtPrecio2.getText()));
								cpa.setMaximo(Integer.valueOf(txtMaximo.getText()));
								cpa.setPosicionProductoEnLaTabla(posicionPersonaEnLaTabla);
								cpa.setFecha(Date.valueOf(txtFecha.getText()));
									if(cpa.agregarDetalle() == true){
										actualizarDetalles();
										limpiar();
										prueba();
									}
							}else{
								JOptionPane.showMessageDialog(null, "No puede superar el Stock M�ximo de productos","Informaci�n",JOptionPane.INFORMATION_MESSAGE);
							}
						}else{
							JOptionPane.showMessageDialog(null, "Introduzca solo n�meros","Informaci�n",JOptionPane.INFORMATION_MESSAGE);
						}
					}else{
						JOptionPane.showMessageDialog(null, "Introduzca solo n�meros","Informaci�n",JOptionPane.INFORMATION_MESSAGE);
					}
				}else{
					JOptionPane.showMessageDialog(null, "Introduzca solo n�meros","Informaci�n",JOptionPane.INFORMATION_MESSAGE);	
				}
			}else{
				JOptionPane.showMessageDialog(null, "Introduzca solo n�meros","Informaci�n",JOptionPane.INFORMATION_MESSAGE);
			}
		}catch(Exception e){
			ce.printLog(e.getMessage(), this.getClass().toString());
		}
	}
	public void actualizarDetalles(){
		try{
			tvDetalles.setItems(cpa.obtenerDetalle());
			txTotal.setText(String.valueOf(cpa.getTotal()));
		}catch(Exception e){
			ce.printLog(e.getMessage(), this.getClass().toString());
		}
	}
	public void selecci(){
		Productos p = tvDetalles.getSelectionModel().getSelectedItem();
		if(p != null){
			txtProducto.setText(p.getNombre());
			txtExistentes.setText(p.getExistentes()+"");
			txtPrecio.setText(p.getCosto()+"");
			txtMaximo.setText(p.getMaximo()+"");
			txtPrecio1.setText(p.getPrecio1()+"");
			txtPrecio2.setText(p.getPrecio2()+"");
		}
	}
	public void desaparecer(){
		lvProveedores.setVisible(false);
	}
	

}
