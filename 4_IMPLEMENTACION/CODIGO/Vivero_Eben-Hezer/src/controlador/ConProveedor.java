package controlador;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Pagination;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Text;
import modelo.Proveedores;

public class ConProveedor implements Initializable{
	
	@FXML private Button btnGuardar, btnAlterno, btnModificar, btnButton, btnEliminar;
	@FXML private TextField txtBuscador, txtCodigo, txtTelefono, txtCorreo, txtMunicipio, txtColonia, txtEmpresa, txtContacto,
	txtAPContacto, txtAMContacto, txtNombre, txtAPaterno, txtAMaterno, txtForaneo, txtCForaneo;
	@FXML private TextArea txtDireccion, txtObservaciones, txtCObservaciones;
	@FXML private Label lblMensaje, lblLimpiar, lblInactivos, lblRecupera;
	@FXML private Text lblClientes, txtInactivos;
	@FXML private ImageView imgBasura;
	@FXML private CheckBox ckbInactivos;
	@FXML private ToggleButton tglFisica, tglMoral, toggle;
	@FXML private TabPane tabMoral, tabFisica, tabGeneral;
	@FXML private ComboBox cbEstado;
	@FXML private TableView<Proveedores> tblprov;
	@FXML private TableColumn<Proveedores, String> provColumn, telefonoColumn, correoColumn, ciudadColumn, tipoColumn;
	@FXML Pagination paginador;
	
	private FilteredList<Proveedores> datosBusqueda;
	private ObservableList<Proveedores> datos;
	private Proveedores c;
	private int filasXPagina;
	private Errores ce;
	private Ventanas ventanas;
	
	public ConProveedor(){
		ventanas = Ventanas.getInstancia();
		c = new Proveedores();
		filasXPagina = 10;
		datos = FXCollections.observableArrayList();
		ce = new Errores();
	}
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		tglFisica.setSelected(true);
		tglFisica.setVisible(true);
		txtInactivos.setVisible(false);
		lblRecupera.setVisible(false);
		imgBasura.setVisible(false);
		btnAlterno.setVisible(false);
		tabMoral.setVisible(false);
		cbEstado.setValue("Selecciona el estado");
		cbEstado.getItems().addAll("Baja California Sur", "Baja California", "Sonora", "Chihuahua", "Sinaloa", "Durango", "Coahuila", "Nuevo Le�n", "Tamaulipas",
			"Zacatecas", "San Luis Potos�", "Nayarit", "Jalisco", "Aguas Calientes", "Colima", "Michoac�n", "Guanajuato", "Guerrero", "Veracruz", "Oaxaca", "Chiapas",
			"Tabasco", "Campeche", "Yucat�n", "Quintana Roo", "Quer�taro", "Hidalgo", "Estado de M�xico", "Tlaxcala", "Puebla", "Morelos", "Distrito Federal");
		
		provColumn.setCellValueFactory(new PropertyValueFactory<Proveedores, String>("nombre"));
		telefonoColumn.setCellValueFactory(new PropertyValueFactory<Proveedores, String>("telefono"));
		correoColumn.setCellValueFactory(new PropertyValueFactory<Proveedores, String>("correo"));
		ciudadColumn.setCellValueFactory(new PropertyValueFactory<Proveedores, String>("municipio"));
		tipoColumn.setCellValueFactory(new PropertyValueFactory<Proveedores, String>("tipo"));
		
		try {
			this.llenarTabla(true);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			ce.printLog(e.getMessage(), this.getClass().toString());
		}
	}
	
	private Node createPage(int pageIndex){
		if(filasXPagina > 0){
			int fromIndex = pageIndex * filasXPagina;
			int toIndex = Math.min(fromIndex + filasXPagina, datosBusqueda.size());
			tblprov.setItems(FXCollections.observableArrayList(datosBusqueda.subList(fromIndex, toIndex)));
			
		}else{
			tblprov.setItems(null);
			paginador.setPageCount(0);
		}
		return new BorderPane(tblprov);
	}
	
	public void llenarTabla(boolean estatus) throws SQLException{
		try{
			datos = c.getProveedor(estatus);
			datosBusqueda = new FilteredList<>(datos);
			int result = datosBusqueda.size()/filasXPagina;
			if(result < 10){
				paginador.setPageCount(1);
			}
			paginador.setPageCount(result);
			System.out.println(datosBusqueda.size()/filasXPagina);
			paginador.setPageFactory((Integer pagina) -> createPage(pagina));
			lblMensaje.setText("Total: "+datos.size() + " registros.");
		}catch(Exception e){
			ce.printLog(e.getMessage(), this.getClass().toString());
			lblMensaje.setText("Se ha producido un error al recuperar los datos");
		}
	}
	
	@FXML public void aparecerMoral(){
		if(tglMoral.isSelected()){
			tabGeneral.setVisible(true);
			btnGuardar.setVisible(false);
			btnAlterno.setVisible(true);
			tabMoral.setVisible(true);
			tabFisica.setVisible(false);
			tglFisica.setDisable(true);
		}else{
			tabGeneral.setVisible(false);
			btnGuardar.setVisible(true);
			btnAlterno.setVisible(false);
			tglFisica.setDisable(false);
			tabMoral.setVisible(false);
			tabFisica.setVisible(false);
		}
	}
	
	@FXML public void aparecerFisica(){
		if(tglFisica.isSelected()){
			tabGeneral.setVisible(true);
			btnGuardar.setVisible(true);
			btnAlterno.setVisible(false);
			tabFisica.setVisible(true);
			tabMoral.setVisible(false);
			tglMoral.setDisable(true);
		}else{
			tabGeneral.setVisible(false);
			btnGuardar.setVisible(false);
			btnAlterno.setVisible(true);
			tglMoral.setDisable(false);
			tabFisica.setVisible(false);
			tabMoral.setVisible(false);
		}
		
	}
	
	/*metodo para aparecer los clientes inactivos*/
	@FXML public void click_inactivos() throws SQLException{
		if(ckbInactivos.isSelected()){
			Limpiar();
			llenarTabla(false);
			btnAlterno.setVisible(false);
			lblRecupera.setVisible(true);
			imgBasura.setVisible(true);
			btnGuardar.setVisible(false);
			btnEliminar.setVisible(false);
			lblClientes.setVisible(false);
			txtInactivos.setVisible(true);
		}else{
			Limpiar();
			llenarTabla(true);
			lblRecupera.setVisible(false);
			imgBasura.setVisible(false);
			btnGuardar.setVisible(true);
			btnEliminar.setVisible(true);
			lblClientes.setVisible(true);
			txtInactivos.setVisible(false);
		}
	}
/*fin del metodo*/
	
	@FXML public void limpiarCampos(){
		this.Limpiar();
	}
	
	public void alternar(){
		try{
			if(txtCodigo.getText().trim().isEmpty()){
				if(txtTelefono.getText().trim().isEmpty()|txtCorreo.getText().trim().isEmpty()|cbEstado.getSelectionModel().getSelectedItem() == null|
						txtMunicipio.getText().trim().isEmpty()|txtColonia.getText().trim().isEmpty()|txtDireccion.getText().trim().isEmpty()|txtEmpresa.getText().trim().isEmpty()|
						txtContacto.getText().trim().isEmpty()|txtAPContacto.getText().trim().isEmpty()|txtAMContacto.getText().trim().isEmpty()|
						txtCObservaciones.getText().trim().isEmpty()){
					lblMensaje.setText("Faltan datos por llenar para guardar");
				}else{
					String num =  txtTelefono.getText();
					Pattern p = Pattern.compile("[A-Za-z.@_-~#]");
					Matcher m = p.matcher(num);
					if(!m.find()){
						String nume =  txtCorreo.getText();
						Pattern pe = Pattern.compile("^[\\w-]+(\\.[\\w-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
						Matcher me = pe.matcher(nume);
						if(me.find()){
							c = new Proveedores();
							c.setTelefono(new SimpleStringProperty(txtTelefono.getText()));
							c.setCorreo(new SimpleStringProperty(txtCorreo.getText()));
							c.setEstado(cbEstado.getSelectionModel().getSelectedItem().toString());
							c.setMunicipio(new SimpleStringProperty(txtMunicipio.getText()));
							c.setColonia(new SimpleStringProperty(txtColonia.getText()));
							c.setDireccion(new SimpleStringProperty(txtDireccion.getText()));
							c.setNombre_empresa(new SimpleStringProperty(txtEmpresa.getText()));
							c.setNombre_contacto(new SimpleStringProperty(txtContacto.getText()));
							c.setApellidop(new SimpleStringProperty(txtAPContacto.getText()));
							c.setApellidom(new SimpleStringProperty(txtAMContacto.getText()));
							c.setObservaciones(new SimpleStringProperty(txtCObservaciones.getText()));
							
							boolean yes = c.insertarMoral();
							if(yes){
								llenarTabla(true);
								lblMensaje.setText("Se guardo correctamente");
								Limpiar();
							}else{
								lblMensaje.setText("�No se pudo guardar!");
							}
						}else{
							lblMensaje.setText("Introduzca un correo valido");
						}
					}else{
						lblMensaje.setText("El campo telefono solo utiliza numeros");
					}
				}
			}else{
				if(txtTelefono.getText().trim().isEmpty()|txtCorreo.getText().trim().isEmpty()|cbEstado.getSelectionModel().getSelectedItem() == null|
						txtMunicipio.getText().trim().isEmpty()|txtColonia.getText().trim().isEmpty()|txtDireccion.getText().trim().isEmpty()|
						txtEmpresa.getText().trim().isEmpty()|txtContacto.getText().trim().isEmpty()|txtAPContacto.getText().trim().isEmpty()|txtAMContacto.getText().trim().isEmpty()|
						txtCObservaciones.getText().trim().isEmpty()){
					lblMensaje.setText("Faltan datos por llenar para modificar");
				}else{
					String num =  txtTelefono.getText();
					Pattern p = Pattern.compile("[A-Za-z.@_-~#]");
					Matcher m = p.matcher(num);
					if(!m.find()){
						String nume =  txtCorreo.getText();
						Pattern pe = Pattern.compile("^[\\w-]+(\\.[\\w-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
						Matcher me = pe.matcher(nume);
						if(me.find()){
							c = new Proveedores();
							c.setId_proveedor(new SimpleIntegerProperty(Integer.valueOf(txtCodigo.getText())));
							c.setTelefono(new SimpleStringProperty(txtTelefono.getText()));
							c.setCorreo(new SimpleStringProperty(txtCorreo.getText()));
							c.setEstado(cbEstado.getSelectionModel().getSelectedItem().toString());
							c.setMunicipio(new SimpleStringProperty(txtMunicipio.getText()));
							c.setColonia(new SimpleStringProperty(txtColonia.getText()));
							c.setDireccion(new SimpleStringProperty(txtDireccion.getText()));
							c.setNombre_empresa(new SimpleStringProperty(txtEmpresa.getText()));
							c.setNombre_contacto(new SimpleStringProperty(txtContacto.getText()));
							c.setApellidop(new SimpleStringProperty(txtAPContacto.getText()));
							c.setApellidom(new SimpleStringProperty(txtAMContacto.getText()));
							c.setObservaciones(new SimpleStringProperty(txtCObservaciones.getText()));
							c.setCod_empresa(new SimpleIntegerProperty(Integer.valueOf(txtForaneo.getText())));
							
							boolean yes = c.modificar();
							if(yes){
								llenarTabla(true);
								lblMensaje.setText("Se modific� correctamente");
								Limpiar();
							}else{
								lblMensaje.setText("No se pudo modificar");
							}
						}else{
							lblMensaje.setText("Introduzca un correo valido");
						}
					}else{
						lblMensaje.setText("El campo telefono solo utiliza numeros");
					}
				}
			}
		}catch(Exception e){
			ce.printLog(e.getMessage(), this.getClass().toString());
		}
	}
	
	@FXML public void guardar(){
		try{
			if(txtCodigo.getText().trim().isEmpty()){
				if(txtTelefono.getText().trim().isEmpty()|txtCorreo.getText().trim().isEmpty()|cbEstado.getSelectionModel().getSelectedItem() == null|
						txtMunicipio.getText().trim().isEmpty()|txtColonia.getText().trim().isEmpty()|txtDireccion.getText().trim().isEmpty()|
						txtNombre.getText().trim().isEmpty()|txtAPaterno.getText().trim().isEmpty()|txtAMaterno.getText().trim().isEmpty()|
						txtObservaciones.getText().trim().isEmpty()){
					lblMensaje.setText("Faltan datos por llenar");
				}else{
					String num =  txtTelefono.getText();
					Pattern p = Pattern.compile("[A-Za-z.@_-~#]");
					Matcher m = p.matcher(num);
					if(!m.find()){
						String nume =  txtCorreo.getText();
						Pattern pe = Pattern.compile("^[\\w-]+(\\.[\\w-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
						Matcher me = pe.matcher(nume);
						if(me.find()){
							c = new Proveedores();
							c.setTelefono(new SimpleStringProperty(txtTelefono.getText()));
							c.setCorreo(new SimpleStringProperty(txtCorreo.getText()));
							c.setEstado(cbEstado.getSelectionModel().getSelectedItem().toString());
							c.setMunicipio(new SimpleStringProperty(txtMunicipio.getText()));
							c.setColonia(new SimpleStringProperty(txtColonia.getText()));
							c.setDireccion(new SimpleStringProperty(txtDireccion.getText()));
							c.setNombre(new SimpleStringProperty(txtNombre.getText()));
							c.setApellido_paterno(new SimpleStringProperty(txtAPaterno.getText()));
							c.setApellido_materno(new SimpleStringProperty(txtAMaterno.getText()));
							c.setObservations(new SimpleStringProperty(txtObservaciones.getText()));
							
							boolean yes = c.insertar();
							if(yes){
								llenarTabla(true);
								lblMensaje.setText("Se guardo correctamente");
								Limpiar();
							}else{
								lblMensaje.setText("No se pudo guardar");
							}
						}else{
							lblMensaje.setText("Introduzca un correo valido");
						}
					}else{
						lblMensaje.setText("El campo telefono solo utiliza numeros");
					}
				}
			}else{
				if(txtTelefono.getText().trim().isEmpty()|txtCorreo.getText().trim().isEmpty()|cbEstado.getSelectionModel().getSelectedItem() == null|
						txtMunicipio.getText().trim().isEmpty()|txtColonia.getText().trim().isEmpty()|txtDireccion.getText().trim().isEmpty()|
						txtNombre.getText().trim().isEmpty()|txtAPaterno.getText().trim().isEmpty()|txtAMaterno.getText().trim().isEmpty()|
						txtObservaciones.getText().trim().isEmpty()){
					lblMensaje.setText("Faltan datos por llenar");
				}else{
					String num =  txtTelefono.getText();
					Pattern p = Pattern.compile("[A-Za-z.@_-~#]");
					Matcher m = p.matcher(num);
					if(!m.find()){
						String nume =  txtCorreo.getText();
						Pattern pe = Pattern.compile("^[\\w-]+(\\.[\\w-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
						Matcher me = pe.matcher(nume);
						if(me.find()){
							c = new Proveedores();
							c.setId_proveedor(new SimpleIntegerProperty(Integer.valueOf(txtCodigo.getText())));
							c.setTelefono(new SimpleStringProperty(txtTelefono.getText()));
							c.setCorreo(new SimpleStringProperty(txtCorreo.getText()));
							c.setEstado(cbEstado.getSelectionModel().getSelectedItem().toString());
							c.setMunicipio(new SimpleStringProperty(txtMunicipio.getText()));
							c.setColonia(new SimpleStringProperty(txtColonia.getText()));
							c.setDireccion(new SimpleStringProperty(txtDireccion.getText()));
							c.setNombre(new SimpleStringProperty(txtNombre.getText()));
							c.setApellidop(new SimpleStringProperty(txtAPaterno.getText()));
							c.setApellidom(new SimpleStringProperty(txtAMaterno.getText()));
							c.setObservations(new SimpleStringProperty(txtObservaciones.getText()));
							c.setCod_proveedor(new SimpleIntegerProperty(Integer.valueOf(txtCForaneo.getText())));
							
							boolean yes = c.modificarFisico();
							if(yes){
								llenarTabla(true);
								lblMensaje.setText("Se modific� correctamente");
								Limpiar();
							}else{
								lblMensaje.setText("�No se pudo modificar!");
							}
						}else{
							lblMensaje.setText("Introduzca un correo valido");
						}
					}else{
						lblMensaje.setText("El campo telefono solo utiliza numeros");
					}
				}
			}
		}catch(Exception e){
			ce.printLog(e.getMessage(), this.getClass().toString());
		}
	}
	
	@FXML public void eliminar() throws SQLException{
		if(txtCodigo.getText().isEmpty()){
			lblMensaje.setText("Debe seleccionar un registro");
		}else{
			c = new Proveedores();
			c.setId_proveedor(new SimpleIntegerProperty(Integer.valueOf(txtCodigo.getText())));
			if(c.eliminar() == true){
				this.llenarTabla(true);
				Limpiar();
				lblMensaje.setText("Registro eliminado");
			}else{
				lblMensaje.setText("Se ha producido un error en el servidor");
			}
		}
	}
	
	public void Limpiar(){
		txtAMaterno.clear();
		txtAMContacto.clear();
		txtAPaterno.clear();
		txtAPContacto.clear();
		txtBuscador.clear();
		txtCForaneo.clear();
		txtCObservaciones.clear();
		txtCodigo.clear();
		txtColonia.clear();
		txtContacto.clear();
		txtCorreo.clear();
		txtDireccion.clear();
		txtEmpresa.clear();
		txtForaneo.clear();
		txtMunicipio.clear();
		txtNombre.clear();
		txtObservaciones.clear();
		txtTelefono.clear();
		cbEstado.getSelectionModel().select(-1);
	}
	
	@FXML public void click_tableview(){
		if(tblprov.getSelectionModel().getSelectedItem() != null){
			c = tblprov.getSelectionModel().getSelectedItem();
			txtCodigo.setDisable(false);
			txtCodigo.setEditable(false);
			txtCodigo.setText(c.getId_proveedor()+"");
			txtTelefono.setText(c.getTelefono());
			txtCorreo.setText(c.getCorreo());
			cbEstado.getSelectionModel().select(c.getEstado());
			txtMunicipio.setText(c.getMunicipio());
			txtColonia.setText(c.getColonia());
			txtDireccion.setText(c.getDireccion());
			txtNombre.setText(c.getNombre());
			txtAPaterno.setText(c.getApellido_paterno());
			txtAMaterno.setText(c.getApellido_materno());
			txtObservaciones.setText(c.getObservaciones());
			txtCObservaciones.setText(c.getObservaciones());
			txtCForaneo.setText(c.getCod_proveedor()+"");
			txtEmpresa.setText(c.getNombre_empresa());
			txtContacto.setText(c.getNombre_contacto());
			txtAPContacto.setText(c.getApellidop());
			txtAMContacto.setText(c.getApellidom());
			txtCObservaciones.setText(c.getObservaciones());
			txtForaneo.setText(c.getCod_empresa()+"");
			if(c.getTipo().equals("Fisica")){
				tglFisica.setDisable(false);
				tglFisica.setSelected(true);
				tglMoral.setDisable(true);
				tabMoral.setVisible(false);
				tabFisica.setVisible(true);
				tabGeneral.setVisible(true);
			}else{
				tglMoral.setDisable(false);
				tglMoral.setSelected(true);
				tglFisica.setDisable(true);
				tabFisica.setVisible(false);
				tabMoral.setVisible(true);
				tabGeneral.setVisible(true);
			}
		}
		
	}
	@FXML public void buscarTexto() throws SQLException{
		if(txtBuscador.getText().trim().isEmpty()){
			datosBusqueda = new FilteredList<> (datos);
			filasXPagina = 10;
			paginador.setPageCount(datosBusqueda.size()/filasXPagina);
			paginador.setPageFactory((Integer pagina) -> createPage(pagina));
			lblMensaje.setText(datosBusqueda.size()+" Registros en la base de datos");
		}else{
			try{
				datosBusqueda.setPredicate(Film->Film.getNombre().toLowerCase().contains(txtBuscador.getText().toLowerCase()));
				if(datosBusqueda.size()< 10)
					filasXPagina = datosBusqueda.size();
				else
					filasXPagina = 10;
				paginador.setPageCount(datosBusqueda.size()/filasXPagina);
				paginador.setPageFactory((Integer pagina)-> createPage(pagina));
				int resultado = datosBusqueda.size();
				lblMensaje.setText(resultado + " resultados encontrados...");
			}catch(Exception e){
				llenarTabla(true);
				lblMensaje.setText("No se encontraron resultados ");
				filasXPagina = 0;
				paginador.setPageCount(filasXPagina);
				paginador.setPageFactory((Integer pagina) -> createPage(pagina));
			}
		}
	}
	
}
