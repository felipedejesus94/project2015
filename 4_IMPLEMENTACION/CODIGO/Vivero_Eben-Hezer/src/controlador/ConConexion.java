package controlador;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

import org.controlsfx.control.ButtonBar;
import org.controlsfx.control.ButtonBar.ButtonType;
import org.controlsfx.control.action.AbstractAction;
import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.Dialog;

import vista.Main;
import modelo.Conexion;
import modelo.Usuario;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;

public class ConConexion implements Initializable{
	private Conexion conn;
	private Usuario usuario;
	private Stage stage;
	private Ventanas ventana;
	public static String nivel;
	//private String usernameResult, passwordResult;
	
	@FXML TextField txtBaseDatos, txtUsuario, txtContrase�a, txtPuerto, txtDireccion, txtUser, txtPass;
	@FXML Label lblMensaje, lblRegistro;
	@FXML TitledPane acrConfiguracion, acrRegistro;
	@FXML Accordion acCordion;
	@FXML Button btnEntrar, btnRegistrar, btnCancelar, btnRespaldar, btnBuscar;
	private EncryptTest en;
	private Errores ce;
	
	public ConConexion(){
		ventana = Ventanas.getInstancia();
		en = new EncryptTest();
		ce = new Errores();
	}
	
	@FXML
	public void click_btnConectar(){
		conn = Conexion.getInstancia();
		if(txtBaseDatos.getText().isEmpty() == false & txtUsuario.getText().isEmpty() == false & txtContrase�a.getText().isEmpty() == false){
			conn.setUsuario(txtUsuario.getText().trim());
			conn.setbd(txtBaseDatos.getText().trim());
			conn.setContrasenia(txtContrase�a.getText().trim());
			conn.setPuerto(txtPuerto.getText().trim());
			conn.setDireccion(txtDireccion.getText().trim());
		}
		String mensaje = conn.conectar();
		lblMensaje.setText(mensaje);
		
	}
	
	@FXML
	public void click_Acceso(){
		if(txtUser.getText().isEmpty() | txtPass.getText().isEmpty()){
			lblMensaje.setText("�Faltan datos por ingresar!");
		}else{
			if(usuario == null){
				usuario = new Usuario();
			}
			usuario.setNombre(txtUser.getText());
			usuario.setContrasenia(txtPass.getText());
			boolean resultado = usuario.Existe(true);
			if(resultado){
				ventana=Ventanas.getInstancia();
				ventana.setPrimaryStage(Main.getPrimaryStage());
				usuario.getNombre();
				usuario.getId_usuario();
				System.out.println(usuario.getId_usuario());
				ventana.asignarEscena("../vista/fxml/Principal.fxml", "menu", usuario.getPuesto(), usuario.getNombre(), usuario.getId_usuario());
			}else{
				System.out.println("Usuario no valido");
			}
		}
	}
	
	@FXML
	public void click_titleServidor(){
		stage=Main.getPrimaryStage();
		acrConfiguracion.setExpanded(false);
		dialogRegistro();
		
		
	}
	
		final TextField username = new TextField();
		final PasswordField password = new PasswordField();
		final Action actionLogin = new AbstractAction("Continuar") {
		    // This method is called when the login button is clicked ...
			@Override
			public void handle(ActionEvent ae) {
				Dialog d = (Dialog) ae.getSource();
		        // Do the login here.
		        d.hide();
			}
		};

		
	 public void dialogRegistro(){
			
		    // Create the custom dialog.
		    Dialog dlg = new Dialog(stage, "Confirmaci�n");

		    GridPane grid = new GridPane();
		    grid.setHgap(10);
		    grid.setVgap(10);
		    grid.setPadding(new Insets(0, 10, 0, 10));

		    username.setPromptText("Usuario");
		    password.setPromptText("Password");

		    grid.add(new Label("Usuario:"), 0, 0);
		    grid.add(username, 1, 0);
		    grid.add(new Label("Contrase�a:"), 0, 1);
		    grid.add(password, 1, 1);

		    ButtonBar.setType(actionLogin, ButtonType.OK_DONE);
		    actionLogin.disabledProperty().set(true);

		    // Do some validation (using the Java 8 lambda syntax).
		    username.textProperty().addListener((observable, oldValue, newValue) -> {
		        actionLogin.disabledProperty().set(newValue.trim().isEmpty());
		    });

		    dlg.setMasthead("�Eres administrador?, si no lo eres, elige cancelar");
		    dlg.setContent(grid);
		    dlg.getActions().addAll(actionLogin, Dialog.Actions.CANCEL);

		    // Request focus on the username field by default.
		    Platform.runLater(() -> username.requestFocus());
		    dlg.show();
		    if((username.getText().isEmpty() | (password.getText().isEmpty()))){
				lblMensaje.setText("Faltan Datos por ingresar");
			}else{
				if(usuario == null){
					usuario = new Usuario();
				}
				usuario.setNombre(username.getText());
				usuario.setContrasenia(password.getText());
				
				boolean resultado = usuario.Existe(true);
				usuario.getPuesto();
				if(resultado == false && usuario.getPuesto() == null){
					System.out.println("no valido");
				}else{
					System.out.println(usuario.getPuesto());
					if(usuario.getPuesto().equals("Administrador")){
						acrConfiguracion.setExpanded(true);
						lblMensaje.setText("Datos Correctos");
						username.clear();
						password.clear();
					}else{
						lblMensaje.setText("Acceso denegado. No tiene permios para ingresar");
						username.clear();
						password.clear();
					}
				}

			}
	 }

	
	@FXML public void cancelar(){
		System.exit(0);
	}
	
	@FXML public void respaldar(){
		if(txtBaseDatos.getText().trim().isEmpty()){
			lblMensaje.setText("Espere...");
			try {
				String ip = "localhost";
				String puerto = "5432";
				String usuario = "postgres";
				String base = "eben_hezer";
				String contrasena = "coldplay";
				
				ProcessBuilder pbuilder;
		        pbuilder = new ProcessBuilder( "c:\\Program Files\\PostgreSQL\\9.4\\bin\\pg_dump", "-h", ip, "-p",puerto , "-U",usuario , "-C", "-d", base, "-f","C:\\demo\\vivero_eben-hezer.txt");
		        pbuilder.environment().put( "PGPASSWORD", contrasena);
		        pbuilder.redirectErrorStream(true);
		        //Ejecuta el proceso
		        
		        Process p = pbuilder.start();
		        p.waitFor();
		        en.encriptar("C:\\demo\\vivero_eben-hezer.txt");
		        lblMensaje.setText("Respaldo Exitoso, se guardo en "+ "C:\\demo\\"+base+".txt");
		        
		    } catch (Exception e) {
		    	ce.printLog(e.getMessage(), this.getClass().toString());
		    }
		}else{
			if(txtBaseDatos.getText().trim().isEmpty()|txtContrase�a.getText().trim().isEmpty()|
					txtDireccion.getText().trim().isEmpty()|txtPuerto.getText().trim().isEmpty()|
					txtUsuario.getText().trim().isEmpty()){
				lblMensaje.setText("Faltan Datos para completar la acci�n");
			}else{
				try {
					String ip = txtDireccion.getText();
					String puerto = txtPuerto.getText();
					String usuario = txtUsuario.getText();
					String base = txtBaseDatos.getText();
					String contrasena = txtContrase�a.getText();
					
					ProcessBuilder pbuilder;
			        pbuilder = new ProcessBuilder( "c:\\Program Files\\PostgreSQL\\9.4\\bin\\pg_dump", "-h", ip, "-p",puerto , "-U",usuario , "-C", "-d", base, "-f","C:\\demo\\"+base+".txt");
			        pbuilder.environment().put( "PGPASSWORD", contrasena);
			        pbuilder.redirectErrorStream(true);
			        //Ejecuta el proceso
			        
			        Process p = pbuilder.start();
			        p.waitFor();
			        lblMensaje.setText("Respaldo Exitoso se guardo en "+ "C:\\demo\\"+base+".txt");
			        System.out.println("C:\\demo\\"+base+".txt");
			        
			    } catch (Exception e) {
			    	ce.printLog(e.getMessage(), this.getClass().toString());
			    }
			}
		}
	}
	
	@FXML public void buscar(){
		ventana = Ventanas.getInstancia();
		ventana.setPrimaryStage(Main.getPrimaryStage());
		ventana.Backup("../vista/fxml/Backup.fxml", "Backups");
	}
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		acCordion.setExpandedPane(acrRegistro);
	}
}
