package controlador;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.Key;
import java.security.MessageDigest;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

public class Encriptar {
	public static String Encriptar(String texto) {
		File archivoaencriptar = new File(texto);

        try {
        	KeyGenerator kf = KeyGenerator.getInstance("DES");
			SecretKey llave = kf.generateKey();
			
            Cipher cipher = Cipher.getInstance("DES");
            cipher.init(Cipher.ENCRYPT_MODE, llave);

            FileOutputStream fos = new FileOutputStream(archivoaencriptar);
			BufferedOutputStream bos = new BufferedOutputStream(fos);
			CipherOutputStream cos = new CipherOutputStream(bos, cipher);
			ObjectOutputStream oos = new ObjectOutputStream(cos);
			
			oos.close();

        } catch (Exception ex) {
        	ex.printStackTrace();
        }
        
		return texto;
}

public static String Desencriptar(String textoEncriptado) throws Exception {

        File desencriptar = new File(textoEncriptado);

        try {
        	KeyGenerator kf = KeyGenerator.getInstance("DES");
			SecretKey llave = kf.generateKey(); 
			
        	Cipher desCipher = Cipher.getInstance("DES");
			desCipher .init(Cipher.DECRYPT_MODE, llave);
			
			// Create stream
			FileInputStream fis = new FileInputStream(desencriptar);
			BufferedInputStream bis = new BufferedInputStream(fis);
			CipherInputStream cis = new CipherInputStream(bis, desCipher);
			ObjectInputStream ois = new ObjectInputStream(cis);
			
			ois.close();

        } catch (Exception ex) {
        	ex.printStackTrace();
        }
		return textoEncriptado;
        
}

	
	public static void main (String [] args) throws Exception{
		String encriptado = Encriptar("c:/demo/Elia.txt");
		System.out.println(encriptado);
		//String desencriptado = Desencriptar("c:/demo/coldplay.txt");
		//System.out.println(desencriptado);
	}

}
