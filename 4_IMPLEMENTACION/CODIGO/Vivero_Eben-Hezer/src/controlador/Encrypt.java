package controlador;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.TreeMap;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class Encrypt {

	public static void main(String args[]) throws Exception {
		encriptar("C:\\demo\\vivero_eben-hezer.txt");
		
	}

	public static boolean encriptar(String ruta){
		try{
		File archivo = new File(ruta);
		FileInputStream archivo_a_encriptar = new FileInputStream(archivo);
		archivo = new File(archivo.getAbsolutePath());
		FileOutputStream archivo_a_desencriptar = new FileOutputStream(archivo);
		byte k[] = "contra87".getBytes();   
		SecretKeySpec contrasenia = new SecretKeySpec(k,"DES");  

		Cipher decrypter =  Cipher.getInstance("DES/ECB/PKCS5Padding");  
		decrypter.init(Cipher.DECRYPT_MODE, contrasenia);  
		CipherInputStream crypter_salida=new CipherInputStream(archivo_a_encriptar, decrypter);
		byte[] buffer = new byte[1024];
		int lector=0;
		
			while((lector=crypter_salida.read(buffer))!=-1) {
			lector=crypter_salida.read(buffer);
				archivo_a_desencriptar.write(buffer,0,lector);     
			}
		
		
		crypter_salida.close();
		archivo_a_desencriptar.flush();
		archivo_a_desencriptar.close();
		return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	
	}
	
	public static void desencriptar(String file) throws Exception{
		File archivo = new File(file);
		FileInputStream archivo_a_encriptar = new FileInputStream(archivo);
		archivo =  new File(archivo.getAbsolutePath());
		FileOutputStream archivo_a_desencriptar = new FileOutputStream(archivo);
		byte k[] = "contra87".getBytes();   
		SecretKeySpec contrasenia = new SecretKeySpec(k,"DES"); 
		Cipher crypter =  Cipher.getInstance("DES/ECB/PKCS5Padding");  
		crypter.init(Cipher.ENCRYPT_MODE, contrasenia);  
		CipherOutputStream contador =new CipherOutputStream(archivo_a_desencriptar, crypter);
		
		byte[] buffer = new byte[1024];
		int lector;
		
		while((lector=archivo_a_encriptar.read(buffer))!=-1)  
		contador.write(buffer,0,lector);     
		archivo_a_encriptar.close();
		contador.flush();
		contador.close();
		
	}
}
