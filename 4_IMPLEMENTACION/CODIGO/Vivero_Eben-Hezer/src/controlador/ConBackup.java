package controlador;

import java.io.File;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.FileChooser.ExtensionFilter;

public class ConBackup {
	@FXML Button btnSalir, btnBuscar, btnDesencriptar;
	@FXML TextField txtRuta;
	@FXML Label lblMensaje;
	
	private Encrypt e;
	
	private Stage primaryStage;
	
	public ConBackup(){
		e = new Encrypt();
	}
	
	public void buscar(){
		FileChooser fileChooser = new FileChooser();
		 fileChooser.setTitle("Buscar archivo");
		 fileChooser.getExtensionFilters().addAll(
		         new ExtensionFilter("Text Files", "*.*"));
		 File selectedFile = fileChooser.showOpenDialog(null);
		if (selectedFile != null) {
			txtRuta.setText(selectedFile.getAbsolutePath());
		}
	}
	public void desencriptar() throws Exception{
		if(txtRuta.getText().trim().isEmpty()){
			lblMensaje.setText("Introduzca una ruta");
		}else{
			if(e.encriptar(txtRuta.getText())){
				lblMensaje.setText("Se Desencriptó el archivo");
			}else{
				lblMensaje.setText("Ocurrio un problema");
			}
		}
	}
	public void salir(){
		primaryStage.close();
	}
	public void setPrimaryStage(Stage dialogStage) {
		// TODO Auto-generated method stub
		this.primaryStage=dialogStage;
	}

	public Stage getPrimaryStage() {
		return primaryStage;
	}
}
