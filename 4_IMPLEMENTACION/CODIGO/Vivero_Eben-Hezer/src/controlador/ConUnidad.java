package controlador;

import java.net.URL;
import java.sql.SQLException;
import java.util.Observable;
import java.util.ResourceBundle;

import modelo.UnidadMedida;
import javafx.beans.InvalidationListener;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class ConUnidad implements Initializable{
	@FXML private Button btnGuardar, btnCancelar, btnNuevo, btnEliminar;
	@FXML private Label lblMensaje, lblRecuperar;
	@FXML private TextField txtNombre, txtId;
	@FXML private ListView<UnidadMedida> lvUnidad;
	@FXML private CheckBox ckbInactivos;
	
	private UnidadMedida um;
	private ObservableList<UnidadMedida> lista;
	private Stage stage;
	private Errores ce;
	
	public ConUnidad(){
		um = new UnidadMedida();
		lista = FXCollections.observableArrayList();
		ce = new Errores();
	}
	
	@FXML public void cancelar(){
		stage.close();
	}
	
	public void limpiar(){
		txtNombre.clear();
		txtId.clear();
	}
	@FXML public void recuperar(){
		try{
			um.setId_unidad(new SimpleIntegerProperty(Integer.valueOf(txtId.getText())));
			if(um.recuperar()){
				llenarListView(false);
				limpiar();
				lblMensaje.setText("Se recupero el elemento");
			}
		}catch(Exception e){
			e.printStackTrace();
			ce.printLog(e.getMessage(), this.getClass().toString());
		}
	}
	@FXML public void activar() throws SQLException{
		if(ckbInactivos.isSelected()){
			lblRecuperar.setVisible(true);
			btnGuardar.setVisible(false);
			btnNuevo.setVisible(false);
			btnEliminar.setVisible(false);
			llenarListView(false);
			nuevo();
		}else{
			lblRecuperar.setVisible(false);
			btnGuardar.setVisible(true);
			btnNuevo.setVisible(true);
			btnEliminar.setVisible(true);
			llenarListView(true);
			nuevo();
		}
	}
	@FXML public void guardar(){
		System.out.println("entro");
		try{
			if(txtId.getText().trim().isEmpty()){
				System.out.println("estoy vacio");
				um.setNombre_unidad(new SimpleStringProperty(txtNombre.getText()));
				if(um.ingresar()){
					lblMensaje.setText("Se registr� correctamente");
					llenarListView(true);
					limpiar();
				}
			}
			if(! txtId.getText().trim().isEmpty()){
				System.out.println("estoy lleno");
				um.setId_unidad(new SimpleIntegerProperty(Integer.valueOf(txtId.getText())));
				um.setNombre_unidad(new SimpleStringProperty(txtNombre.getText()));
				if(um.modificar()){
					lblMensaje.setText("Se modific� correctamente");
					llenarListView(true);
					limpiar();
				}
			}
		}catch(Exception e){
			ce.printLog(e.getMessage(), this.getClass().toString());
		}
	}
	
	@FXML public void eliminar(){
		try{
			um.setId_unidad(new SimpleIntegerProperty(Integer.valueOf(txtId.getText())));
			if(um.eliminar()){
				llenarListView(true);
				limpiar();
				lblMensaje.setText("Se elimino el elemento");
			}
		}catch(Exception e){
			e.printStackTrace();
			ce.printLog(e.getMessage(), this.getClass().toString());
		}
	}
	@FXML public void nuevo(){
		limpiar();
	}
	
	@FXML public void clickListView(){
		UnidadMedida um = lvUnidad.getSelectionModel().getSelectedItem();
		if(um != null){
			txtId.setText(um.getId_unidad()+"");
			txtNombre.setText(um.getNombre_unidad());
		}
	}

	public void setDialogStage(Stage dialogStage) {
		// TODO Auto-generated method stub
		this.stage=dialogStage;
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		lblRecuperar.setVisible(false);
		try {
			llenarListView(true);
		} catch (SQLException e) {
			ce.printLog(e.getMessage(), this.getClass().toString());
			e.printStackTrace();
		}
	}
	
	public void llenarListView(boolean estatus) throws SQLException{
		lista = um.getUnidad(estatus);
		lvUnidad.setItems(lista);
	}

}
