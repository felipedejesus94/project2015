package controlador;

import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JOptionPane;

import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;
import modelo.Clientes;
import modelo.Productos;
import modelo.Usuario;
import modelo.Venta;

public class ConVentas implements Initializable{
	
	@FXML private TextField txtBuscador, txtFolio, txtCliente, txtEmpleado, txtProducto, txtExistentes,
	txtPrecio, txtCantidad, txtCodEmpleado, txtCodCliente;
	@FXML private Button btnGuardar, btnCancelar, btnTicket, btnBuscar, btnEliminar, btnModificar,
	btnAgregar, btnNueva;
	@FXML private Text txTotal;
	@FXML private ListView<Productos> lvProductos;
	@FXML private ListView<Clientes> lvClientes;
	@FXML private TableView<Productos> tvDetalles;
	@FXML private TableColumn<Productos, String> tcProducto, tcPrecio, tcCantidad, tcSubtotal;
	
	private ObservableList<Productos> listaDetalle;
	private ObservableList<Productos> elementos;
	private ObservableList<Clientes> clientes;
	private FilteredList<Productos> productosBusqueda;
	private FilteredList<Clientes> clientesBusqueda;
	private Productos p;
	private Clientes c;
	static String nombre;
	public static Integer codigo;
	//private Usuario usuario;
	private Venta v;
	private Principal prin;
	private int posicionPersonaEnLaTabla;
	
	public ConVentas() {
		elementos = FXCollections.observableArrayList();
		p = new Productos();
		c = new Clientes();
		v = new Venta();
		prin = new Principal();
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		try{
			txtCodCliente.setVisible(false);
			txtCodEmpleado.setVisible(false);
			btnAgregar.setDisable(true);
			btnModificar.setDisable(true);
			btnEliminar.setDisable(true);
			folioVenta();
			txtCodEmpleado.setText(codigo+"");
			txtEmpleado.setText(nombre);
			elementos = p.getProductos(true);
			lvProductos.setItems(elementos);
			clientes = c.getFilms(true);
			lvClientes.setItems(clientes);
			lvClientes.setVisible(false);
			llenarTabla();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void llenarTabla(){
		tcProducto.setCellValueFactory(new PropertyValueFactory<Productos, String>("nombre"));
		if(txtCodCliente.getText().trim().isEmpty()){
			tcPrecio.setCellValueFactory(new PropertyValueFactory<Productos, String>("precio1"));
			System.out.println("no es cliente en la tabla");
		}else{
			tcPrecio.setCellValueFactory(new PropertyValueFactory<Productos, String>("precio2"));
			System.out.println("si es cliente en la tabla");
		}
		tcCantidad.setCellValueFactory(new PropertyValueFactory<Productos, String>("cantidad"));
		tcSubtotal.setCellValueFactory(new PropertyValueFactory<Productos, String>("subtotal"));
		listaDetalle = FXCollections.observableArrayList();
		tvDetalles.setItems(listaDetalle);
		
		final ObservableList<Productos> tablaPersonaSeleccionada = tvDetalles.getSelectionModel().getSelectedItems();
		tablaPersonaSeleccionada.addListener(selectorTablaPersonas);
	}
	
	public void folioVenta(){
		if(v.Existe(true)){
			txtFolio.setText(v.getVenta_id()+"");
		}
	}
	@FXML public void refrescar(){
		clean();
		folioVenta();
		debeIngresar();
	}
	@FXML public void seleccionar(){
		try{
			Productos p = lvProductos.getSelectionModel().getSelectedItem();
			if(p != null){
				txtProducto.setText(p.getNombre());
				txtExistentes.setText(p.getExistentes()+"");
				if(txtCodCliente.getText().trim().isEmpty()){
					txtPrecio.setText(p.getPrecio1()+"");
					//llenarTabla();
				}else{
					txtPrecio.setText(p.getPrecio2()+"");
					//llenarTabla();
				}
			}
			debeIngresar();
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void debeIngresar(){
		if(txtCliente.getText().trim().isEmpty()){
			System.out.println("debe ingresar un cliente");
			btnAgregar.setDisable(true);
			btnEliminar.setDisable(true);
			btnModificar.setDisable(true);
		}else{
			btnAgregar.setDisable(false);
			btnEliminar.setDisable(false);
			btnModificar.setDisable(false);
		}
	}
	
	public void clean(){
		txtFolio.clear();
		txtCliente.clear();
		txtCodCliente.clear();
		limpiar();
		llenarTabla();
	}
	
	@FXML public void selecci(){
		Productos p = tvDetalles.getSelectionModel().getSelectedItem();
		if(p != null){
			txtProducto.setText(p.getNombre());
			txtExistentes.setText(p.getExistentes()+"");
			if(txtCodCliente.getText().trim().isEmpty()){
				txtPrecio.setText(p.getPrecio1()+"");
			}else{
				txtPrecio.setText(p.getPrecio2()+"");
			}
		}
	}
	
	public void limpiar(){
		txtCantidad.clear();
		txtPrecio.clear();
		txtExistentes.clear();
		txtProducto.clear();
	}
	
	public void prueba() throws SQLException{
		elementos = p.getProductos(true);
		lvProductos.setItems(elementos);
	}
	
	public void agregarProducto(){
		try{
			String num = txtCantidad.getText();
			Pattern p = Pattern.compile("[A-Za-z.@_-~#]");
			Matcher ma = p.matcher(num);
			if(!ma.find()){
				int cantidad = Integer.parseInt(txtCantidad.getText());
				int existencias = Integer.parseInt(txtExistentes.getText());
				if(cantidad > 0 && cantidad <= existencias){
					v.setProductos(lvProductos.getSelectionModel().getSelectedItem());
					System.out.println(listaDetalle + " en prueba");
					v.setListaDetalles(listaDetalle);
					v.setCantidad(Integer.valueOf(txtCantidad.getText()));
					v.setExistentes(Integer.valueOf(txtExistentes.getText()));
					v.setVenta_id(Integer.valueOf(txtFolio.getText()));
					if(txtCodCliente.getText().trim().isEmpty()){
						System.out.println("no es cliente");
					}else{
						v.setCliente_id(Integer.valueOf(txtCodCliente.getText()));
					}
					v.setEmpleado_id(Integer.valueOf(txtCodEmpleado.getText()));
					//posicionPersonaEnLaTabla = prueba.size();
					System.out.println(posicionPersonaEnLaTabla+" primero");
					v.setPosicionProductoEnTabla(posicionPersonaEnLaTabla);
						if(v.agregarDetalle() == true){
							actualizarDetalles();
							limpiar();
							prueba();
						}
				}
			}else{
				JOptionPane.showMessageDialog(null, "Introduzca solo numeros","Información",JOptionPane.INFORMATION_MESSAGE);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	@FXML public void ejecutarVenta(){
		if(! txtCodCliente.getText().trim().isEmpty()){
			if(v.guardar()){
				JOptionPane.showMessageDialog(null, "Se ha registrado la venta","Información",JOptionPane.INFORMATION_MESSAGE);
				clean();
				debeIngresar();
			}
		}else{
			if(v.agragar()){
				clean();
				debeIngresar();
			}
		}
	}
	
	@FXML public void doble(){
		clean();
		debeIngresar();
	}
	
	@FXML public void editarProducto(){
		try{
			String num = txtCantidad.getText();
			Pattern p = Pattern.compile("[A-Za-z.@_-~#]");
			Matcher ma = p.matcher(num);
			if(!ma.find()){
				int cantidad = Integer.parseInt(txtCantidad.getText());
				int existencias = Integer.parseInt(txtExistentes.getText());
				if(cantidad > 0 && cantidad <= existencias){
					v.setProductos(tvDetalles.getSelectionModel().getSelectedItem());
					System.out.println(lvProductos.getSelectionModel().getSelectedItem()+" lista");
					System.out.println(tvDetalles.getSelectionModel().getSelectedItem()+" tabla");
					System.out.println(listaDetalle + " en prueba");
					v.setListaDetalles(listaDetalle);
					v.setCantidad(Integer.valueOf(txtCantidad.getText()));
					v.setExistentes(Integer.valueOf(txtExistentes.getText()));
					v.setVenta_id(Integer.valueOf(txtFolio.getText()));
					if(txtCodCliente.getText().trim().isEmpty()){
					System.out.println("no es cliente");
					}else{
						v.setCliente_id(Integer.valueOf(txtCodCliente.getText()));
					}
					v.setEmpleado_id(Integer.valueOf(txtCodEmpleado.getText()));
					//posicionPersonaEnLaTabla = prueba.size();
					System.out.println(posicionPersonaEnLaTabla+" primero");
					System.out.println(listaDetalle.size()+"talla uno...");
					v.setPosicionProductoEnTabla(posicionPersonaEnLaTabla);
					
					if(v.editarDetalle() == true){
						actualizarDetalles();
						limpiar();
						prueba();
					}
				}else{
					System.out.println("pues no tiene existentes");
				}
			}else{
				JOptionPane.showMessageDialog(null, "Introduzca solo numeros","Información",JOptionPane.INFORMATION_MESSAGE);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	@FXML public void eliminarProducto(){
		try{
			int cantidad = Integer.parseInt(txtCantidad.getText());
			int existencias = Integer.parseInt(txtExistentes.getText());
			if(cantidad > 0 && cantidad <= existencias){
				v.setProductos(tvDetalles.getSelectionModel().getSelectedItem());
				v.setPosicionProductoEnTabla(posicionPersonaEnLaTabla);
				System.out.println(posicionPersonaEnLaTabla);
				if(v.eliminarDetalle() == true){
					actualizarDetalles();
					limpiar();
					prueba();
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}

	public void actualizarDetalles(){
		try{
			tvDetalles.setItems(v.obtenerDetalle());
			txTotal.setText(String.valueOf(v.getTotal()));
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private final ListChangeListener<Productos> selectorTablaPersonas =
            new ListChangeListener<Productos>() {
                @Override
                public void onChanged(ListChangeListener.Change<? extends Productos> c) {
                    ponerPersonaSeleccionada();
                }
	};
	
	public Productos getTablaPersonaSeleccionada(){
		if(tvDetalles != null){
			List<Productos> tabla = tvDetalles.getSelectionModel().getSelectedItems();
			if(tabla.size()==1){
				final Productos camposeleccionado = tabla.get(0);
				return camposeleccionado;
			}
		}
		return null;
	}
	
	private void ponerPersonaSeleccionada(){
		final Productos det = getTablaPersonaSeleccionada();
		posicionPersonaEnLaTabla = listaDetalle.indexOf(det);
		if(det != null){
			//det = tvDetalles.getSelectionModel().getSelectedItem();
			txtProducto.setText(det.getNombre());
			txtExistentes.setText(det.getExistentes()+"");
			if(txtCodCliente.getText().trim().isEmpty()){
				txtPrecio.setText(det.getPrecio1()+"");
			}else{
				txtPrecio.setText(det.getPrecio2()+"");
			}
			txtCantidad.setText(det.getCantidad()+"");
		}
	}
	
	@FXML public void buscarProducto(){
		try{
			if(! txtBuscador.getText().trim().isEmpty()){
				productosBusqueda = new FilteredList<Productos>(elementos);
				productosBusqueda.setPredicate(Film -> Film.getNombre().toLowerCase().contains(txtBuscador.getText().toLowerCase()));
				lvProductos.setItems(productosBusqueda);
			}else{
				lvProductos.setItems(elementos);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public Productos getP() {
		return p;
	}

	public void setP(Productos p) {
		this.p = p;
	}

	public ObservableList<Productos> obtenerDetalle(){
		return listaDetalle;
	}

	
	
	@FXML public void aparecer() throws SQLException{
		lvClientes.setVisible(true);
		actualiz();
	}
	@FXML public void desaparecer(){
		lvClientes.setVisible(false);
	}
	public void actualiz() throws SQLException{
		clientes = c.getFilms(true);
		lvClientes.setItems(clientes);
	}
	@FXML public void buscarCliente(){
		try{
			if(! txtCliente.getText().trim().isEmpty()){
				clientesBusqueda = new FilteredList<Clientes>(clientes);
				clientesBusqueda.setPredicate(Film -> Film.getNombre().toLowerCase().contains(txtCliente.getText().toLowerCase()));
				lvClientes.setItems(clientesBusqueda);
			}else{
				lvClientes.setItems(clientes);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@FXML public void selecc(){
		try{
			Clientes c = lvClientes.getSelectionModel().getSelectedItem();
			if(c != null){
				txtCliente.setText(c.getNombre());
				txtCodCliente.setText(c.getId_cliente()+"");
				lvClientes.setVisible(false);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
}
