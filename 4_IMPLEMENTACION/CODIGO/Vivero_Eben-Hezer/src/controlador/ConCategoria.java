package controlador;

import java.io.File;
import java.net.URL;
import java.util.Observable;
import java.util.ResourceBundle;

import modelo.Categoria;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class ConCategoria implements Initializable{
	@FXML private Button btnGuardar, btnEliminar, btnNuevo;
	@FXML private Label lblMensaje, lblRecuperar;
	@FXML private TextField txtCategoria, txtId;
	@FXML private CheckBox ckbInactivos;
	@FXML private ListView<Categoria> lvCategoria;
	
	private Categoria c;
	private ObservableList<Categoria> lista;
	private boolean modificar;
	private File file;
	private Stage stage;
	private Errores ce;
	
	public ConCategoria(){
		c = new Categoria();
		lista = FXCollections.observableArrayList();
		modificar = false;
		ce = new Errores();
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		lblRecuperar.setVisible(false);
		try{
			llenarListView(true);
		}catch(Exception e){
			//e.printStackTrace();
			ce.printLog(e.getMessage(), this.getClass().toString());
		}
	}
	
	public void llenarListView(boolean estatus){
		try{
			//File archivo = new File("c:/demo/coldplay.txt");
			lista = c.getCategoria(estatus);
			lvCategoria.setItems(lista);
		}catch(Exception e){
			//e.printStackTrace();
			ce.printLog(e.getMessage(), this.getClass().toString());
		}
	}
	
	@FXML private void clickListView(){
		Categoria c = lvCategoria.getSelectionModel().getSelectedItem();
		if(c != null){
			txtCategoria.setText(c.getNombre());
			txtId.setText(c.getId_categoria()+"");
		}
	}
	@FXML public void activar(){
		if(ckbInactivos.isSelected()){
			lblRecuperar.setVisible(true);
			btnGuardar.setVisible(false);
			btnNuevo.setVisible(false);
			btnEliminar.setVisible(false);
			llenarListView(false);
			nuevo();
		}else{
			lblRecuperar.setVisible(false);
			btnGuardar.setVisible(true);
			btnNuevo.setVisible(true);
			btnEliminar.setVisible(true);
			llenarListView(true);
			nuevo();
		}
	}
	@FXML public void recuperar(){
		try{
			c.setId_categoria(new SimpleIntegerProperty(Integer.valueOf(txtId.getText())));
			if(c.recuperar()){
				llenarListView(false);
				limpiar();
				lblMensaje.setText("Se recupero el elemento");
			}
		}catch(Exception e){
			e.printStackTrace();
			ce.printLog(e.getMessage(), this.getClass().toString());
		}
	}
	
	@FXML public void guardar(){
		try{
			if(txtId.getText().trim().isEmpty()){
				c.setNombre(new SimpleStringProperty(txtCategoria.getText()));
				if(c.ingresar()){
					llenarListView(true);
					limpiar();
					lblMensaje.setText("Se registr� correctamente");
				}
			}else{
				c.setNombre(new SimpleStringProperty(txtCategoria.getText()));
				c.setId_categoria(new SimpleIntegerProperty(Integer.valueOf(txtId.getText())));
				if(c.modificar()){
					llenarListView(true);
					limpiar();
					lblMensaje.setText("Se modific� correctamente");
				}
			}
			
		}catch(Exception e){
			ce.printLog(e.getMessage(), this.getClass().toString());
		}
		
	}
	@FXML public void eliminar(){
		try{
			c.setId_categoria(new SimpleIntegerProperty(Integer.valueOf(txtId.getText())));
			if(c.eliminar()){
				llenarListView(true);
				limpiar();
				lblMensaje.setText("se elimin� el registro");
			}
		}catch(Exception e){
			ce.printLog(e.getMessage(), this.getClass().toString());
		}
	}
	@FXML public void nuevo(){
		limpiar();
	}
	
	public void limpiar(){
		txtCategoria.clear();
		txtId.clear();
	}
	
	@FXML public void cancelar(){
		stage.close();
	}

	public void setDialogStage(Stage dialogStage) {
		// TODO Auto-generated method stub
		this.stage=dialogStage;
	}

}
