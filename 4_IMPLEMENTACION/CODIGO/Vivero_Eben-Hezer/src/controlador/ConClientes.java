package controlador;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Pagination;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Text;
import modelo.Clientes;

public class ConClientes implements Initializable{
	
/*Regin para enlazar los controles de la vista*/
	@FXML private Button btnGuardar, btnAlterno, btnEliminar;
	@FXML private TextField txtBuscador, txtCodigo, txtTelefono, txtCorreo, txtMunicipio, txtColonia, txtEmpresa, txtContacto,
	txtAPContacto, txtAMContacto, txtNombre, txtAPaterno, txtAMaterno, txtForaneo, txtCForaneo;
	@FXML private TextArea txtDireccion, txtObservaciones, txtCObservaciones;
	@FXML private Label lblMensaje, lblLimpiar, lblRecupera, lblInactivos, lbl2, lbl3, lbl4, lbl5, lbl, lbl6, lbl7, lbl8, lbl9, lbl10, lbl11;
	@FXML private Text lblClientes, lbl1;
	@FXML private ToggleButton tglFisica, tglMoral, toggle;
	@FXML private TabPane tabMoral, tabFisica, tabGeneral;
	@FXML private ComboBox cbEstado;
	@FXML private CheckBox ckbInactivo;
	@FXML private ImageView imgBasura;
	@FXML private TableView<Clientes> tblClientes;
	@FXML private TableColumn<Clientes, String> clienteColumn, telefonoColumn, correoColumn, ciudadColumn, tipoColumn;
	@FXML private Pagination paginador;
/*fin de la region*/
	
/*Region de las variables*/
	private int filasXPagina;
	private FilteredList<Clientes> datosBusqueda;
	private ObservableList<Clientes> datos;
	private Clientes c;
	private Ventanas ventanas;
	private Errores ce;
/*fin de la region*/
	
/*constructor*/
	public ConClientes(){
		ventanas = Ventanas.getInstancia();
		c = new Clientes();
		filasXPagina = 10;
		datos = FXCollections.observableArrayList();
		ce = new Errores();
	}

/*metodos con los que se inicia la ventana*/
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		try{
			noVisible();
			tglFisica.setSelected(true);
			lblInactivos.setVisible(false);
			lblRecupera.setVisible(false);
			imgBasura.setVisible(false);
			btnAlterno.setVisible(false);
			//tabFisica.setVisible(false);
			tabMoral.setVisible(false);
			cbEstado.setValue("Selecciona el estado");
			cbEstado.getItems().addAll("Baja California Sur", "Baja California", "Sonora", "Chihuahua", "Sinaloa", "Durango", "Coahuila", "Nuevo Le�n", "Tamaulipas",
				"Zacatecas", "San Luis Potos�", "Nayarit", "Jalisco", "Aguas Calientes", "Colima", "Michoac�n", "Guanajuato", "Guerrero", "Veracruz", "Oaxaca", "Chiapas",
				"Tabasco", "Campeche", "Yucat�n", "Quintana Roo", "Quer�taro", "Hidalgo", "Estado de M�xico", "Tlaxcala", "Puebla", "Morelos", "Distrito Federal");
			
			clienteColumn.setCellValueFactory(new PropertyValueFactory<Clientes, String>("nombre"));
			telefonoColumn.setCellValueFactory(new PropertyValueFactory<Clientes, String>("telefono"));
			correoColumn.setCellValueFactory(new PropertyValueFactory<Clientes, String>("correo"));
			ciudadColumn.setCellValueFactory(new PropertyValueFactory<Clientes, String>("municipio"));
			tipoColumn.setCellValueFactory(new PropertyValueFactory<Clientes, String>("tipo"));
			
			this.llenarTabla(true);
		}catch(Exception e){
			//e.printStackTrace();
			ce.printLog(e.getMessage(), this.getClass().toString());
		}
		
	}
/*fin del metodo*/
	
/*metodo para asignar el numero de registros a la tabla*/
	private Node createPage(int pageIndex){
		if(filasXPagina > 0){
			int fromIndex = pageIndex * filasXPagina;
			int toIndex = Math.min(fromIndex + filasXPagina, datosBusqueda.size());
			tblClientes.setItems(FXCollections.observableArrayList(datosBusqueda.subList(fromIndex, toIndex)));
			
		}else{
			tblClientes.setItems(null);
			paginador.setPageCount(0);
		}
		return new BorderPane(tblClientes);
	}
/*fin del metodo*/
	
/*metodo para llenar el TableView*/
	public void llenarTabla(boolean estatus) {
		try{
			datos = c.getFilms(estatus);
			datosBusqueda = new FilteredList<>(datos);
			System.out.println(datos);
			paginador.setPageCount(datosBusqueda.size()/filasXPagina);
			System.out.println(datosBusqueda.size()/filasXPagina);
			paginador.setPageFactory((Integer pagina) -> createPage(pagina));
			lblMensaje.setText("Total: "+datos.size() + " registros.");
		}catch(Exception e){
			ce.printLog(e.getMessage(), this.getClass().toString());
			lblMensaje.setText("Se ha producido un error al recuperar los datos");
		}
	}
/*fin del metodo*/
	
/*acciones que se realizan si el toogleButton(moral) esta presionado*/
	@FXML public void aparecerMoral(){
		if(tglMoral.isSelected()){
			tabGeneral.setVisible(true);
			btnGuardar.setVisible(false);
			btnAlterno.setVisible(true);
			tabMoral.setVisible(true);
			tabFisica.setVisible(false);
			tglFisica.setDisable(true);
		}else{
			tabGeneral.setVisible(false);
			btnGuardar.setVisible(true);
			btnAlterno.setVisible(false);
			tglFisica.setDisable(false);
			tabMoral.setVisible(false);
			tabFisica.setVisible(false);
		}
	}
/*fin del metodo*/
	
/*acciones a realizar si el ToggleButton (fisico) esta presionado*/
	@FXML public void aparecerFisica(){
		if(tglFisica.isSelected()){
			tabGeneral.setVisible(true);
			btnGuardar.setVisible(true);
			btnAlterno.setVisible(false);
			tabFisica.setVisible(true);
			tabMoral.setVisible(false);
			tglMoral.setDisable(true);
		}else{
			tabGeneral.setVisible(false);
			btnGuardar.setVisible(false);
			btnAlterno.setVisible(true);
			tglMoral.setDisable(false);
			tabFisica.setVisible(false);
			tabMoral.setVisible(false);
		}
	}
/*fin del metodo*/
	
/*metodo para aparecer los clientes inactivos*/
	@FXML public void click_inactivos() throws SQLException{
		if(ckbInactivo.isSelected()){
			limpiar();
			llenarTabla(false);
			btnAlterno.setVisible(false);
			lblRecupera.setVisible(true);
			imgBasura.setVisible(true);
			btnGuardar.setVisible(false);
			btnEliminar.setVisible(false);
			lblClientes.setVisible(false);
			lblInactivos.setVisible(true);
		}else{
			limpiar();
			llenarTabla(true);
			lblRecupera.setVisible(false);
			imgBasura.setVisible(false);
			btnGuardar.setVisible(true);
			btnEliminar.setVisible(true);
			lblClientes.setVisible(true);
			lblInactivos.setVisible(false);
		}
	}
/*fin del metodo*/
	
/*metodo para recuperar un cliente inactivo*/
	@FXML public void click_recuperar() throws SQLException{
		if(txtCodigo.getText().isEmpty()){
			lblMensaje.setText("Debe seleccionar un registro");
		}else{
			c = new Clientes();
			c.setId_cliente(new SimpleIntegerProperty(Integer.valueOf(txtCodigo.getText())));
			if(c.recuperar() == true){
				this.llenarTabla(false);
				limpiar();
				lblMensaje.setText("Registro recuperado");
			}else{
				lblMensaje.setText("Se ha producido un error en el servidor");
			}
		}
	}
/*fin del metodo*/
	public void visibe(){
		lbl1.setVisible(true);
		lbl2.setVisible(true);
		lbl3.setVisible(true);
		lbl4.setVisible(true);
		lbl5.setVisible(true);
		lbl6.setVisible(true);
		lbl7.setVisible(true);
		lbl8.setVisible(true);
		lbl9.setVisible(true);
		lbl10.setVisible(true);
		lbl11.setVisible(true);
	}
	public void noVisible(){
		lbl1.setVisible(false);
		lbl2.setVisible(false);
		lbl3.setVisible(false);
		lbl4.setVisible(false);
		lbl5.setVisible(false);
		lbl6.setVisible(false);
		lbl7.setVisible(false);
		lbl8.setVisible(false);
		lbl9.setVisible(false);
		lbl10.setVisible(false);
		lbl11.setVisible(false);
	}
	
/*metodo para guardar o modificar un cliente moral*/
	public void alternar(){
		try{
			if(txtCodigo.getText().trim().isEmpty()){
				if(txtTelefono.getText().trim().isEmpty()|txtCorreo.getText().trim().isEmpty()|cbEstado.getSelectionModel().getSelectedItem() == null|
						txtMunicipio.getText().trim().isEmpty()|txtColonia.getText().trim().isEmpty()|txtDireccion.getText().trim().isEmpty()|txtEmpresa.getText().trim().isEmpty()|
						txtContacto.getText().trim().isEmpty()|txtAPContacto.getText().trim().isEmpty()|txtAMContacto.getText().trim().isEmpty()|
						txtCObservaciones.getText().trim().isEmpty()){
						visibe();
					lblMensaje.setText("Faltan datos por llenar para guardar");
				}else{ 
					String num =  txtTelefono.getText();
					Pattern p = Pattern.compile("[A-Za-z.@_-~#]");
					Matcher m = p.matcher(num);
					if(!m.find()){
						String nume =  txtCorreo.getText();
						Pattern pe = Pattern.compile("^[\\w-]+(\\.[\\w-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
						Matcher me = pe.matcher(nume);
						if(me.find()){
							c = new Clientes();
							c.setTelefono(new SimpleStringProperty(txtTelefono.getText()));
							c.setCorreo(new SimpleStringProperty(txtCorreo.getText()));
							c.setEstado(cbEstado.getSelectionModel().getSelectedItem().toString());
							c.setMunicipio(new SimpleStringProperty(txtMunicipio.getText()));
							c.setColonia(new SimpleStringProperty(txtColonia.getText()));
							c.setDireccion(new SimpleStringProperty(txtDireccion.getText()));
							c.setNombre_empresa(new SimpleStringProperty(txtEmpresa.getText()));
							c.setNombre_contacto(new SimpleStringProperty(txtContacto.getText()));
							c.setApellidop(new SimpleStringProperty(txtAPContacto.getText()));
							c.setApellidom(new SimpleStringProperty(txtAMContacto.getText()));
							c.setObservaciones(new SimpleStringProperty(txtCObservaciones.getText()));
							
							boolean yes = c.insertarMoral();
							if(yes){
								llenarTabla(true);
								lblMensaje.setText("Se guardo correctamente");
								limpiar();
							}else{
								lblMensaje.setText("�No se pudo guardar!");
							}
						}else{
							lblMensaje.setText("Introduzca un correo valido");
						}
					}else{
						lblMensaje.setText("El campo telefono solo utiliza numeros");
					}
					
				}
			}else{
				if(txtTelefono.getText().trim().isEmpty()|txtCorreo.getText().trim().isEmpty()|cbEstado.getSelectionModel().getSelectedItem() == null|
						txtMunicipio.getText().trim().isEmpty()|txtColonia.getText().trim().isEmpty()|txtDireccion.getText().trim().isEmpty()|
						txtEmpresa.getText().trim().isEmpty()|txtContacto.getText().trim().isEmpty()|txtAPContacto.getText().trim().isEmpty()|txtAMContacto.getText().trim().isEmpty()|
						txtCObservaciones.getText().trim().isEmpty()){
					lblMensaje.setText("Faltan datos por llenar para modificar");
				}else{
					String num =  txtTelefono.getText();
					Pattern p = Pattern.compile("[A-Za-z.@_-~#]");
					Matcher m = p.matcher(num);
					if(!m.find()){
						String nume =  txtCorreo.getText();
						Pattern pe = Pattern.compile("^[\\w-]+(\\.[\\w-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
						Matcher me = pe.matcher(nume);
						if(me.find()){
							c = new Clientes();
							c.setId_cliente(new SimpleIntegerProperty(Integer.valueOf(txtCodigo.getText())));
							c.setTelefono(new SimpleStringProperty(txtTelefono.getText()));
							c.setCorreo(new SimpleStringProperty(txtCorreo.getText()));
							c.setEstado(cbEstado.getSelectionModel().getSelectedItem().toString());
							c.setMunicipio(new SimpleStringProperty(txtMunicipio.getText()));
							c.setColonia(new SimpleStringProperty(txtColonia.getText()));
							c.setDireccion(new SimpleStringProperty(txtDireccion.getText()));
							c.setNombre_empresa(new SimpleStringProperty(txtEmpresa.getText()));
							c.setNombre_contacto(new SimpleStringProperty(txtContacto.getText()));
							c.setApellidop(new SimpleStringProperty(txtAPContacto.getText()));
							c.setApellidom(new SimpleStringProperty(txtAMContacto.getText()));
							c.setObservaciones(new SimpleStringProperty(txtCObservaciones.getText()));
							c.setCod_empresa(new SimpleIntegerProperty(Integer.valueOf(txtForaneo.getText())));
							
							boolean yes = c.modificar();
							if(yes){
								llenarTabla(true);
								lblMensaje.setText("Se modific� correctamente");
								limpiar();
							}else{
								lblMensaje.setText("No se pudo modificar");
							}
						}else{
							lblMensaje.setText("Introduzca un correo valido");
						}
					}else{
						lblMensaje.setText("El campo telefono solo utiliza numeros");
					}
					
				}
			}
		}catch(Exception e){
			//e.printStackTrace();
			ce.printLog(e.getMessage(), this.getClass().toString());
		}
	}
/*fin del metodo*/
	
/*metodo para guardar o modificar un cliente fisico*/
	@FXML public void guardar(){
		try{
			if(txtCodigo.getText().trim().isEmpty()){
				if(txtTelefono.getText().trim().isEmpty()|txtCorreo.getText().trim().isEmpty()|cbEstado.getSelectionModel().getSelectedItem() == null|
						txtMunicipio.getText().trim().isEmpty()|txtColonia.getText().trim().isEmpty()|txtDireccion.getText().trim().isEmpty()|
						txtNombre.getText().trim().isEmpty()|txtAPaterno.getText().trim().isEmpty()|txtAMaterno.getText().trim().isEmpty()|
						txtObservaciones.getText().trim().isEmpty()){
					lblMensaje.setText("Faltan datos por llenar");
				}else{
					String num = txtTelefono.getText();
					Pattern pa = Pattern.compile("[A-Za-z.@_-~#]");
					Matcher mc = pa.matcher(num);
					if(!mc.find()){
						String corr = txtCorreo.getText();
						Pattern ptr = Pattern.compile("^[\\w-]+(\\.[\\w-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
						Matcher mtc = ptr.matcher(corr);
						if(mtc.find()){
							c = new Clientes();
							c.setTelefono(new SimpleStringProperty(txtTelefono.getText()));
							c.setCorreo(new SimpleStringProperty(txtCorreo.getText()));
							c.setEstado(cbEstado.getSelectionModel().getSelectedItem().toString());
							c.setMunicipio(new SimpleStringProperty(txtMunicipio.getText()));
							c.setColonia(new SimpleStringProperty(txtColonia.getText()));
							c.setDireccion(new SimpleStringProperty(txtDireccion.getText()));
							c.setNombre(new SimpleStringProperty(txtNombre.getText()));
							c.setApellido_paterno(new SimpleStringProperty(txtAPaterno.getText()));
							c.setApellido_materno(new SimpleStringProperty(txtAMaterno.getText()));
							c.setObservations(new SimpleStringProperty(txtObservaciones.getText()));
							
							boolean yes = c.insertar();
							if(yes){
								llenarTabla(true);
								lblMensaje.setText("Se guardo correctamente");
								limpiar();
							}else{
								lblMensaje.setText("No se pudo guardar");
							}
						}else{
							lblMensaje.setText("Introduzca un correo valido");
						}
					}else{
						lblMensaje.setText("El campo t�lefono solo utiliza numeros");
					}
					
				}
			}else{
				if(txtTelefono.getText().trim().isEmpty()|txtCorreo.getText().trim().isEmpty()|cbEstado.getSelectionModel().getSelectedItem() == null|
						txtMunicipio.getText().trim().isEmpty()|txtColonia.getText().trim().isEmpty()|txtDireccion.getText().trim().isEmpty()|
						txtNombre.getText().trim().isEmpty()|txtAPaterno.getText().trim().isEmpty()|txtAMaterno.getText().trim().isEmpty()|
						txtObservaciones.getText().trim().isEmpty()){
					lblMensaje.setText("Faltan datos por llenar");
				}else{
					String num = txtTelefono.getText();
					Pattern pa = Pattern.compile("[A-Za-z.@_-~#]");
					Matcher mc = pa.matcher(num);
					if(!mc.find()){
						String corr = txtCorreo.getText();
						Pattern ptr = Pattern.compile("^[\\w-]+(\\.[\\w-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
						Matcher mtc = ptr.matcher(corr);
						if(mtc.find()){
							c = new Clientes();
							c.setId_cliente(new SimpleIntegerProperty(Integer.valueOf(txtCodigo.getText())));
							c.setTelefono(new SimpleStringProperty(txtTelefono.getText()));
							c.setCorreo(new SimpleStringProperty(txtCorreo.getText()));
							c.setEstado(cbEstado.getSelectionModel().getSelectedItem().toString());
							c.setMunicipio(new SimpleStringProperty(txtMunicipio.getText()));
							c.setColonia(new SimpleStringProperty(txtColonia.getText()));
							c.setDireccion(new SimpleStringProperty(txtDireccion.getText()));
							c.setNombre(new SimpleStringProperty(txtNombre.getText()));
							c.setApellidop(new SimpleStringProperty(txtAPaterno.getText()));
							c.setApellidom(new SimpleStringProperty(txtAMaterno.getText()));
							c.setObservations(new SimpleStringProperty(txtObservaciones.getText()));
							c.setCod_cliente(new SimpleIntegerProperty(Integer.valueOf(txtCForaneo.getText())));
							
							boolean yes = c.modificarFisico();
							if(yes){
								llenarTabla(true);
								lblMensaje.setText("Se modific� correctamente");
								limpiar();
							}else{
								lblMensaje.setText("�No se pudo modificar!");
							}
						}else{
							lblMensaje.setText("Introduzca un correo valido");
						}
					}else{
						lblMensaje.setText("El campo t�lefono solo utiliza numeros");
					}
					
				}
			}
		}catch(Exception e){
			//e.printStackTrace();
			ce.printLog(e.getMessage(), this.getClass().toString());
		}
	}
/*fin del metodo*/
		
/*metodo para eliminar un cliente*/
	@FXML public void eliminar() throws SQLException{
		if(txtCodigo.getText().isEmpty()){
			lblMensaje.setText("Debe seleccionar un registro");
		}else{
			c = new Clientes();
			c.setId_cliente(new SimpleIntegerProperty(Integer.valueOf(txtCodigo.getText())));
			if(c.eliminar() == true){
				this.llenarTabla(true);
				limpiar();
				lblMensaje.setText("Registro eliminado");
			}else{
				lblMensaje.setText("Se ha producido un error en el servidor");
			}
		}
	}
/*fin del metodo*/
	
/*metodo para limpiar*/
	public void limpiar(){
		txtCodigo.setDisable(true);
		txtAMaterno.clear();
		txtAMContacto.clear();
		txtAPaterno.clear();
		txtAPContacto.clear();
		txtBuscador.clear();
		txtCForaneo.clear();
		txtCObservaciones.clear();
		txtCodigo.clear();
		txtColonia.clear();
		txtContacto.clear();
		txtCorreo.clear();
		txtDireccion.clear();
		txtEmpresa.clear();
		txtForaneo.clear();
		txtMunicipio.clear();
		txtNombre.clear();
		txtObservaciones.clear();
		txtTelefono.clear();
		cbEstado.getSelectionModel().select(-1);
	}
/*fin del metodo*/
	
/*metodo para subir los valores de la tabla a las cajas de texto*/
	@FXML public void click_tableview(){
		if(tblClientes.getSelectionModel().getSelectedItem() != null){
			c = tblClientes.getSelectionModel().getSelectedItem();
			txtCodigo.setDisable(false);
			txtCodigo.setEditable(false);
			txtCodigo.setText(c.getId_cliente()+"");
			txtTelefono.setText(c.getTelefono());
			txtCorreo.setText(c.getCorreo());
			cbEstado.getSelectionModel().select(c.getEstado());
			txtMunicipio.setText(c.getMunicipio());
			txtColonia.setText(c.getColonia());
			txtDireccion.setText(c.getDireccion());
			txtNombre.setText(c.getNombre());
			txtAPaterno.setText(c.getApellido_paterno());
			txtAMaterno.setText(c.getApellido_materno());
			txtObservaciones.setText(c.getObservaciones());
			txtCObservaciones.setText(c.getObservaciones());
			txtCForaneo.setText(c.getCod_cliente()+"");
			txtEmpresa.setText(c.getNombre_empresa());
			txtContacto.setText(c.getNombre_contacto());
			txtAPContacto.setText(c.getApellidop());
			txtAMContacto.setText(c.getApellidom());
			txtCObservaciones.setText(c.getObservaciones());
			txtForaneo.setText(c.getCod_empresa()+"");
			if(c.getTipo().equals("Fisica")){
				tglFisica.setDisable(false);
				tglFisica.setSelected(true);
				tglMoral.setDisable(true);
				tabMoral.setVisible(false);
				tabFisica.setVisible(true);
				tabGeneral.setVisible(true);
			}else{
				tglMoral.setDisable(false);
				tglMoral.setSelected(true);
				tglFisica.setDisable(true);
				tabFisica.setVisible(false);
				tabMoral.setVisible(true);
				tabGeneral.setVisible(true);
			}
		}
		
	}
/*fin del metodo*/
	
/*metodo para buscar registros*/
	@FXML public void buscarTexto() throws SQLException{
		if(txtBuscador.getText().trim().isEmpty()){
			datosBusqueda = new FilteredList<> (datos);
			filasXPagina = 10;
			paginador.setPageCount(datosBusqueda.size()/filasXPagina);
			paginador.setPageFactory((Integer pagina) -> createPage(pagina));
			lblMensaje.setText(datosBusqueda.size()+" Registros en la base de datos");
		}else{
			try{
				datosBusqueda.setPredicate(Film->Film.getNombre().toLowerCase().contains(txtBuscador.getText().toLowerCase()));
				if(datosBusqueda.size()< 10)
					filasXPagina = datosBusqueda.size();
				else
					filasXPagina = 10;
				paginador.setPageCount(datosBusqueda.size()/filasXPagina);
				paginador.setPageFactory((Integer pagina)-> createPage(pagina));
				int resultado = datosBusqueda.size();
				lblMensaje.setText(resultado + " resultados encontrados...");
			}catch(Exception e){
				llenarTabla(true);
				lblMensaje.setText("No se encontraron resultados ");
				filasXPagina = 0;
				paginador.setPageCount(filasXPagina);
				paginador.setPageFactory((Integer pagina) -> createPage(pagina));
				ce.printLog(e.getMessage(), this.getClass().toString());
			}
		}
	}
/*fin del metodo*/

}

