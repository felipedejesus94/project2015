package controlador;

import java.io.IOException;

import vista.Main;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class Ventanas {
	/*
	 * Atributos
	 */
	private static Ventanas instancia;
	private Stage primaryStage;
	private Scene escena;
	private BorderPane contenedor;
	private BorderPane contenedorDialog;
	private BorderPane subcontenedorDialog;
	
	// #region Constructor
	/*
	 * Constructor privado
	 */
	private Ventanas() {

	}
	
	// #endregion
	
	
	// #region M�todos
	
	/*
	 * Recuperar la instancia de la clase
	 */
	public static Ventanas getInstancia() {
		if(instancia==null){
			instancia= new Ventanas();
		}
		return instancia;
	}
	
	/*
	 * Establecer Escenario principal
	 */
	
	public void setPrimaryStage(Stage primaryStage) {
		this.primaryStage = primaryStage;
	}
	/*
	 * Asignar el men� principal de la ventana
	 */
	
	public void asignarMenu(String ruta, String titulo){
		try {
			FXMLLoader interfaz = new FXMLLoader(getClass().getResource(ruta));			
			contenedor = (BorderPane)interfaz.load();			
			Rectangle2D screenBounds = Screen.getPrimary().getVisualBounds();
			primaryStage.centerOnScreen();
			escena = new Scene(contenedor,screenBounds.getWidth(), 
					screenBounds.getHeight());
			primaryStage.setScene(escena);
			primaryStage.setTitle(titulo);
			primaryStage.initStyle(StageStyle.DECORATED);
			primaryStage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/*
	 * Para cambiar entre escenas
	 */
	
	public void asignarEscena(String ruta, String titulo,String nivel, String nombre, int codigo){
		try {
			Principal.nivel= nivel;
			Principal.nombre = nombre;
			Principal.codigo = codigo;
			FXMLLoader interfaz = new FXMLLoader(getClass().getResource(ruta));
			contenedorDialog = (BorderPane)interfaz.load();			
			primaryStage = Main.getPrimaryStage();
			primaryStage.setTitle(titulo);
			//dialogEscenario.initModality(Modality.WINDOW_MODAL);
			//dialogEscenario.initOwner(primaryStage);			
			escena = new Scene(contenedorDialog);			
			primaryStage.setScene(escena);	
			primaryStage.centerOnScreen();
			//primaryStage.initStyle(StageStyle.DECORATED);
			primaryStage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void asignarAnchorPane(String ruta){
		try {
			FXMLLoader interfaz = new FXMLLoader(getClass().getResource(ruta));
			AnchorPane overviewPage = (AnchorPane)interfaz.load();
			contenedorDialog.setCenter(overviewPage);
			//Scene escena = new Scene(contenedorDialog);
			
			primaryStage.setScene(escena);
			primaryStage.centerOnScreen();
			primaryStage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void asignarBorderCentro(String ruta, String nombre, Integer codigo){
		try{
			ConVentas.nombre=nombre;
			ConVentas.codigo=codigo;
			FXMLLoader interfaz = new FXMLLoader(getClass().getResource(ruta));
			subcontenedorDialog = (BorderPane)interfaz.load();
			contenedorDialog.setCenter(subcontenedorDialog);
			//Scene escena = new Scene(contenedorDialog);
			
			primaryStage.setScene(escena);
			primaryStage.centerOnScreen();
			primaryStage.show();
		} catch (Exception e){
			e.printStackTrace();
		}
	}
	public void asignarBorderSur(String ruta, String nombre, Integer codigo){
		try{
			ConCompra.nombre=nombre;
			ConCompra.codigo=codigo;
			FXMLLoader interfaz = new FXMLLoader(getClass().getResource(ruta));
			subcontenedorDialog = (BorderPane)interfaz.load();
			contenedorDialog.setCenter(subcontenedorDialog);
			//Scene escena = new Scene(contenedorDialog);
			
			primaryStage.setScene(escena);
			primaryStage.centerOnScreen();
			primaryStage.show();
		} catch (Exception e){
			e.printStackTrace();
		}
	}
	
	
	public void categoria(String ruta, String titulo){
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Ventanas.class.getResource(ruta));
			AnchorPane page = (AnchorPane) loader.load();
			
			Stage dialogStage = new Stage();
			dialogStage.setTitle(titulo);
			dialogStage.initModality(Modality.WINDOW_MODAL);
			//dialogStage.initOwner(primaryStage);
			
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);
			
			ConCategoria controller = loader.getController();
			controller.setDialogStage(dialogStage);
			dialogStage.showAndWait();
		}catch (IOException e) {
			e.printStackTrace();
		}
	}
	public void Backup(String ruta, String titulo){
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Ventanas.class.getResource(ruta));
			AnchorPane page = (AnchorPane) loader.load();
			
			Stage dialogStage = new Stage();
			dialogStage.setTitle(titulo);
			dialogStage.initModality(Modality.WINDOW_MODAL);
			//dialogStage.initOwner(primaryStage);
			
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);
			
			ConBackup controller = loader.getController();
			controller.setPrimaryStage(dialogStage);
			dialogStage.showAndWait();
		}catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void unidad_medida(String ruta, String titulo){
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Ventanas.class.getResource(ruta));
			AnchorPane page = (AnchorPane) loader.load();
			
			Stage dialogStage = new Stage();
			dialogStage.setTitle(titulo);
			dialogStage.initModality(Modality.WINDOW_MODAL);
			//dialogStage.initOwner(primaryStage);
			
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);
			
			ConUnidad controller = loader.getController();
			controller.setDialogStage(dialogStage);
			dialogStage.showAndWait();
		}catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}

//try {
	//FXMLLoader loader = new FXMLLoader(Main.class.getResource("Productos.fxml"));
	//AnchorPane overviewPage = (AnchorPane) loader.load();
	//rootLayout.setCenter(overviewPage);
	
	//controlador.Productos controller = loader.getController();
	//controller.setMain(this);
	
//} catch (IOException e) {
	//e.printStackTrace();
//}

