package controlador;

import java.io.*;
import javax.crypto.*;
import javax.crypto.spec.*;
import java.security.*;
import java.security.spec.*;
import java.util.*;

public class EncryptTest {

	public static void main(String args[]) {
		encriptar("c:/demo/vivero_eben-hezer_.txt");
	}
	
	public static void encriptar(String ruta){
		File desFile = new File(ruta);

		// Create data to encrypt
		Map map = new TreeMap(System.getProperties());
		int number = map.size();

		try {

		// Create Key
		KeyGenerator kg = KeyGenerator.getInstance("DES");
		SecretKey secretKey = kg.generateKey();

		// Create Cipher
		Cipher desCipher =
		Cipher.getInstance("DES/ECB/PKCS5Padding");
		desCipher.init(Cipher.ENCRYPT_MODE, secretKey);

		// Create stream
		FileOutputStream fos = new FileOutputStream(desFile);
		BufferedOutputStream bos = new BufferedOutputStream(fos);
		CipherOutputStream cos = new CipherOutputStream(bos, desCipher);
		ObjectOutputStream oos = new ObjectOutputStream(cos);

		// Write objects
		oos.writeObject(map);
		oos.writeInt(number);
		//oos.flush();
		oos.close();

		// Change cipher mode
		desCipher.init(Cipher.DECRYPT_MODE, secretKey);

		// Create stream
		FileInputStream fis = new FileInputStream(desFile);
		BufferedInputStream bis = new BufferedInputStream(fis);
		CipherInputStream cis = new CipherInputStream(bis, desCipher);
		ObjectInputStream ois = new ObjectInputStream(cis);

		// Write objects
		//ois.reset();
		//ois.close();

		// Read objects
		Map map2 = (Map)ois.readObject();
		int number2 = ois.readInt();
		ois.close();

		// Compare original with what was read back
		if (map.equals(map2) && (map.size() == number2)) {
		System.out.println("Se completo correctamente");
		} else {
		System.out.println("Problemas durante el proceso");
		}
		} catch (NoSuchPaddingException e) {
		System.err.println("Padding problem: " + e);
		} catch (NoSuchAlgorithmException e) {
		System.err.println("Invalid algorithm: " + e);
		} catch (InvalidKeyException e) {
		System.err.println("Invalid key: " + e);
		} catch (IOException e) {
		System.err.println("I/O Problem: " + e);
		} catch (ClassNotFoundException e) {
		System.err.println("Class loading Problem: " + e);
		} finally {
		if (desFile.exists()) {
		//desFile.delete();
		}
		}
	}
}
