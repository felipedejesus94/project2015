package controlador;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Pagination;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Text;
import modelo.Clientes;
import modelo.Usuarios;

public class ConUsuario implements Initializable{
	@FXML private  Button btnGuardar, btnNuevo, btnEliminar;
	@FXML private TextField txtCodigo, txtNombre, txtApaterno, txtAmaterno, txtContrasena, txtTelefono,
	txtCorreo, txtMunicipio, txtBuscador, txtColonia;
	@FXML private TextArea txtDireccion;
	@FXML private ComboBox cbPuesto, cbEstado;
	@FXML private CheckBox ckbInactivos;
	@FXML private Label lblLimpiar, lblMensaje, lblInactivos, lblCodigo, lblRecuperar;
	@FXML private TableView<Usuarios> tvUsuarios;
	@FXML private TableColumn<Usuarios, String> tcNombre, tcTelefono, tcCorreo, tcPuesto, tcCiudad, tcDireccion;
	@FXML private Pagination pagination;
	@FXML private ImageView imgBasura;
	@FXML private Text txUsuarios;
	
	private ObservableList<Usuarios> elementos;
	private FilteredList<Usuarios> datosBusqueda;
	private int filasXPagina;
	private Usuarios u;
	private Errores ce;
	
	public ConUsuario(){
		elementos = FXCollections.observableArrayList();
		u = new Usuarios();
		filasXPagina = 10;
		ce = new Errores();
	}
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		lblRecuperar.setVisible(false);
		txtCodigo.setEditable(false);
		txtCodigo.setDisable(true);
		imgBasura.setVisible(false);
		lblInactivos.setVisible(false);
		cbEstado.setValue("Selecciona el estado");
		cbEstado.getItems().addAll("Baja California Sur", "Baja California", "Sonora", "Chihuahua", "Sinaloa", "Durango", "Coahuila", "Nuevo Le�n", "Tamaulipas",
			"Zacatecas", "San Luis Potos�", "Nayarit", "Jalisco", "Aguas Calientes", "Colima", "Michoac�n", "Guanajuato", "Guerrero", "Veracruz", "Oaxaca", "Chiapas",
			"Tabasco", "Campeche", "Yucat�n", "Quintana Roo", "Quer�taro", "Hidalgo", "Estado de M�xico", "Tlaxcala", "Puebla", "Morelos", "Distrito Federal");
		cbPuesto.setValue("Selecciona el puesto");
		cbPuesto.getItems().addAll("Empleado","Administrador");
		try {
			llenarTabla(true);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			ce.printLog(e.getMessage(), this.getClass().toString());
		}
	}

	public void llenarTabla(boolean estatus) throws SQLException{
		tcNombre.setCellValueFactory(new PropertyValueFactory<Usuarios, String>("nombre"));
		tcTelefono.setCellValueFactory(new PropertyValueFactory<Usuarios, String>("telefono"));
		tcCorreo.setCellValueFactory(new PropertyValueFactory<Usuarios, String>("correo"));
		tcPuesto.setCellValueFactory(new PropertyValueFactory<Usuarios, String>("puesto"));
		tcCiudad.setCellValueFactory(new PropertyValueFactory<Usuarios, String>("municipio"));
		tcDireccion.setCellValueFactory(new PropertyValueFactory<Usuarios, String>("direccion"));
		elementos = u.getUsuarios(estatus);

		try{
			elementos = u.getUsuarios(estatus);
			datosBusqueda = new FilteredList<>(elementos);
			int result = datosBusqueda.size()/filasXPagina;
			if(result < 10){
				pagination.setPageCount(1);
			}
			pagination.setPageCount(result);
			System.out.println(datosBusqueda.size()/filasXPagina);
			pagination.setPageFactory((Integer pagina) -> createPage(pagina));
			lblMensaje.setText("Total: "+elementos.size() + " registros.");
		}catch(Exception e){
			e.printStackTrace();
			lblMensaje.setText("Se ha producido un error al recuperar los datos");
			ce.printLog(e.getMessage(), this.getClass().toString());
		}
	}
	
	private Node createPage(int pageIndex){
		if(filasXPagina > 0){
			int fromIndex = pageIndex * filasXPagina;
			int toIndex = Math.min(fromIndex + filasXPagina, datosBusqueda.size());
			tvUsuarios.setItems(FXCollections.observableArrayList(datosBusqueda.subList(fromIndex, toIndex)));
			
		}else{
			tvUsuarios.setItems(null);
			pagination.setPageCount(0);
		}
		return new BorderPane(tvUsuarios);
	}
	
	@FXML public void guardar(){
		try{
			if(txtCodigo.getText().trim().isEmpty()){
				if(txtNombre.getText().trim().isEmpty()|txtApaterno.getText().trim().isEmpty()|txtAmaterno.getText().trim().isEmpty()|
					cbPuesto.getSelectionModel().getSelectedItem() == null|txtContrasena.getText().trim().isEmpty()|
					txtTelefono.getText().trim().isEmpty()|txtCorreo.getText().trim().isEmpty()|cbEstado.getSelectionModel().getSelectedItem() == null|
					txtMunicipio.getText().trim().isEmpty()|txtColonia.getText().trim().isEmpty()|txtDireccion.getText().trim().isEmpty()){
					lblMensaje.setText("Todos los Campos son necesarios");
				}else{
					String num =  txtTelefono.getText();
					Pattern p = Pattern.compile("[A-Za-z.@_-~#]");
					Matcher m = p.matcher(num);
					if(!m.find()){
						String nume =  txtCorreo.getText();
						Pattern pe = Pattern.compile("^[\\w-]+(\\.[\\w-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
						Matcher me = pe.matcher(nume);
						if(me.find()){
							u = new Usuarios();
							u.setNombre(new SimpleStringProperty(txtNombre.getText()));
							u.setaPaterno(new SimpleStringProperty(txtApaterno.getText()));
							u.setaMaterno(new SimpleStringProperty(txtAmaterno.getText()));
							u.setPuesto(cbPuesto.getSelectionModel().getSelectedItem().toString());
							u.setContrasenia(new SimpleStringProperty(txtContrasena.getText()));
							u.setTelefono(new SimpleStringProperty(txtTelefono.getText()));
							u.setCorreo(new SimpleStringProperty(txtCorreo.getText()));
							u.setEstado(cbEstado.getSelectionModel().getSelectedItem().toString());
							u.setMunicipio(new SimpleStringProperty(txtMunicipio.getText()));
							u.setColonia(new SimpleStringProperty(txtColonia.getText()));
							u.setDireccion(new SimpleStringProperty(txtDireccion.getText()));
							boolean yes = u.insertar();
							if(yes){
								limpiar();
								llenarTabla(true);
								lblMensaje.setText("Se guardo correctamente");
							}else{
								lblMensaje.setText("Se produjo un error al guardar los datos");
							}
						}else{
							lblMensaje.setText("Introduzca un correo valido");
						}
					}else{
						lblMensaje.setText("El campo telefono solo utiliza numeros");
					}
				}
			}else{
				if(txtNombre.getText().trim().isEmpty()|txtApaterno.getText().trim().isEmpty()|txtAmaterno.getText().trim().isEmpty()|
						cbPuesto.getSelectionModel().getSelectedItem() == null|txtContrasena.getText().trim().isEmpty()|
						txtTelefono.getText().trim().isEmpty()|txtCorreo.getText().trim().isEmpty()|cbEstado.getSelectionModel().getSelectedItem() == null|
						txtMunicipio.getText().trim().isEmpty()|txtColonia.getText().trim().isEmpty()|txtDireccion.getText().trim().isEmpty()){
						lblMensaje.setText("No puede Modificar un campo a vacio");
					}else{
						String num =  txtTelefono.getText();
						Pattern p = Pattern.compile("[A-Za-z.@_-~#]");
						Matcher m = p.matcher(num);
						if(!m.find()){
							String nume =  txtCorreo.getText();
							Pattern pe = Pattern.compile("^[\\w-]+(\\.[\\w-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
							Matcher me = pe.matcher(nume);
							if(me.find()){
								u = new Usuarios();
								u.setIdUsuario(new SimpleIntegerProperty(Integer.valueOf(txtCodigo.getText())));
								u.setNombre(new SimpleStringProperty(txtNombre.getText()));
								u.setaPaterno(new SimpleStringProperty(txtApaterno.getText()));
								u.setaMaterno(new SimpleStringProperty(txtAmaterno.getText()));
								u.setPuesto(cbPuesto.getSelectionModel().getSelectedItem().toString());
								u.setContrasenia(new SimpleStringProperty(txtContrasena.getText()));
								u.setTelefono(new SimpleStringProperty(txtTelefono.getText()));
								u.setCorreo(new SimpleStringProperty(txtCorreo.getText()));
								u.setEstado(cbEstado.getSelectionModel().getSelectedItem().toString());
								u.setMunicipio(new SimpleStringProperty(txtMunicipio.getText()));
								u.setColonia(new SimpleStringProperty(txtColonia.getText()));
								u.setDireccion(new SimpleStringProperty(txtDireccion.getText()));
								boolean yes = u.modificar();
								if(yes){
									limpiar();
									llenarTabla(true);
									lblMensaje.setText("Se guardo modifico correctamente");
								}else{
									lblMensaje.setText("Se produjo un error al modificar los datos");
								}
							}else{
								lblMensaje.setText("Introduzca un correo valido");
							}
						}else{
							lblMensaje.setText("El campo telefono solo utiliza numeros");
						}
					}
			}
		}catch(Exception e){
			ce.printLog(e.getMessage(), this.getClass().toString());
			e.printStackTrace();
		}
	}
	@FXML public void eliminar() throws SQLException{
		if(txtCodigo.getText().isEmpty()){
			lblMensaje.setText("Debe seleccionar un registro");
		}else{
			u = new Usuarios();
			u.setIdUsuario(new SimpleIntegerProperty(Integer.valueOf(txtCodigo.getText())));
			if(u.eliminar() == true){
				this.llenarTabla(true);
				limpiar();
				lblMensaje.setText("Registro eliminado");
			}else{
				lblMensaje.setText("Se ha producido un error en el servidor");
			}
		}
	}
	
	@FXML private void recuperar() throws SQLException{
		if(txtCodigo.getText().isEmpty()){
			lblMensaje.setText("Debe seleccionar un registro");
		}else{
			u = new Usuarios();
			u.setIdUsuario(new SimpleIntegerProperty(Integer.valueOf(txtCodigo.getText())));
			if(u.recuperar() == true){
				this.llenarTabla(false);
				limpiar();
				lblMensaje.setText("Registro recuperado");
			}else{
				lblMensaje.setText("Se ha producido un error en el servidor");
			}
		}
	}
	
	@FXML public void buscarUsuarios() throws SQLException{
		if(txtBuscador.getText().trim().isEmpty()){
			datosBusqueda = new FilteredList<> (elementos);
			filasXPagina = 10;
			pagination.setPageCount(datosBusqueda.size()/filasXPagina);
			pagination.setPageFactory((Integer pagina) -> createPage(pagina));
			lblMensaje.setText(datosBusqueda.size()+" registros en la base de datos");
		}else{
			try{
				datosBusqueda.setPredicate(Film->Film.getNombre().toLowerCase().contains(txtBuscador.getText().toLowerCase()));
				if(datosBusqueda.size()< 10)
					filasXPagina = datosBusqueda.size();
				else
					filasXPagina = 10;
				pagination.setPageCount(datosBusqueda.size()/filasXPagina);
				pagination.setPageFactory((Integer pagina)-> createPage(pagina));
				int resultado = datosBusqueda.size();
				lblMensaje.setText(resultado + " resultados encontrados...");
			}catch(Exception e){
				e.printStackTrace();
				llenarTabla(true);
				lblMensaje.setText("No se encontraron resultados ");
				filasXPagina = 0;
				pagination.setPageCount(filasXPagina);
				pagination.setPageFactory((Integer pagina) -> createPage(pagina));
			}
		}
		
	}
	@FXML public void nuevo(){
		limpiar();
		txtCodigo.setDisable(true);
	}
	@FXML public void seleccionar(){
		if(tvUsuarios.getSelectionModel().getSelectedItem() != null){
			u = tvUsuarios.getSelectionModel().getSelectedItem();
			txtCodigo.setDisable(false);
			txtCodigo.setText(u.getIdUsuario()+"");
			txtNombre.setText(u.getNombre());
			txtApaterno.setText(u.getaPaterno());
			txtAmaterno.setText(u.getaMaterno());
			cbPuesto.getSelectionModel().select(u.getPuesto());
			txtContrasena.setText(u.getContrasenia());
			txtTelefono.setText(u.getTelefono());
			txtCorreo.setText(u.getCorreo());
			cbEstado.getSelectionModel().select(u.getEstado());
			txtMunicipio.setText(u.getMunicipio());
			txtColonia.setText(u.getColonia());
			txtDireccion.setText(u.getDireccion());
		}
	}
	@FXML public void limpiar(){
		txtCodigo.clear();
		txtNombre.clear();
		txtAmaterno.clear();
		txtApaterno.clear();
		cbPuesto.getSelectionModel().select(-1);
		txtContrasena.clear();
		txtTelefono.clear();
		txtCorreo.clear();
		cbEstado.getSelectionModel().select(-1);
		txtMunicipio.clear();
		txtColonia.clear();
		txtDireccion.clear();
		
	}
	@FXML public void inactivos() throws SQLException{
		if(ckbInactivos.isSelected()){
			llenarTabla(false);
			txUsuarios.setVisible(false);
			lblInactivos.setVisible(true);
			imgBasura.setVisible(true);
			btnGuardar.setVisible(false);
			btnNuevo.setVisible(false);
			btnEliminar.setVisible(false);
			lblRecuperar.setVisible(true);
			limpiar();
		}else{
			llenarTabla(true);
			txUsuarios.setVisible(true);
			lblInactivos.setVisible(false);
			imgBasura.setVisible(false);
			btnGuardar.setVisible(true);
			btnNuevo.setVisible(true);
			btnEliminar.setVisible(true);
			lblRecuperar.setVisible(false);
			limpiar();
		}
	}

}
