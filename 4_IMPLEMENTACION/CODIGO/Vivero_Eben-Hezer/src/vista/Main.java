package vista;
	
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;


public class Main extends Application {
	
	private static Stage primaryStage;
	
	public Main(){
		
	}
	@Override
	public void start(Stage primaryStage) {
		
		try {
			this.primaryStage=primaryStage;
			Parent root = FXMLLoader.load(getClass().getResource("fxml/Conexion.fxml"));
			Scene escena = new Scene(root);
			primaryStage.setTitle("Conexion");
			primaryStage.setScene(escena);
			primaryStage.getIcons().add(new Image("vista/images/logo.png"));
			primaryStage.show();
			primaryStage.setResizable(false);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		launch(args);
	}
	
	
	public static Stage getPrimaryStage(){
		return primaryStage;
	}
}

