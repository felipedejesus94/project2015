package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.StringProperty;

public class Usuario {
	private String nombre, contrasenia, puesto;
	private Integer id_usuario;
	private Boolean estatus;
	private Conexion conn;
	private Connection miconexion;
	private PreparedStatement comando;
	
	public Usuario(){
		this.nombre = null;
		this.contrasenia = null;
		this.puesto = null;
		this.estatus = null;
		
	}
	
	
	
	public String getNombre() {
		return nombre;
	}



	public void setNombre(String nombre) {
		this.nombre = nombre;
	}



	public String getContrasenia() {
		return contrasenia;
	}



	public void setContrasenia(String contrasenia) {
		this.contrasenia = contrasenia;
	}



	public String getPuesto() {
		return puesto;
	}



	public void setPuesto(String puesto) {
		this.puesto = puesto;
	}



	public Boolean getEstatus() {
		return estatus;
	}



	public void setEstatus(Boolean estatus) {
		this.estatus = estatus;
	}



	public boolean Existe(boolean bandera){
		conn = Conexion.getInstancia();
		bandera = false;
		try{
			String sql = "select * from usuario where nombre = ? and contrasenia = ?";
			
			conn.conectar();
			miconexion = conn.getConexion();
			comando = conn.getConexion().prepareStatement(sql);
			comando.setString(1, this.nombre);
			comando.setString(2, this.contrasenia);
			
			ResultSet rs = comando.executeQuery();
			while(rs.next()){
				this.id_usuario=(rs.getInt(1));
				this.puesto=(rs.getString(5));
				bandera = true;
			}
		}catch(Exception e){
			System.out.println("********************ERROR");
			e.printStackTrace();
		}
		finally{
			conn.desconectar();
		}
		return bandera;
	}



	public Integer getId_usuario() {
		return id_usuario;
	}



	public void setId_usuario(Integer id_usuario) {
		this.id_usuario = id_usuario;
	}

}


