package modelo;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.FloatProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Productos {
	private StringProperty  nombre, cientifico, estatus;
	Categoria nombre_categoria;
	UnidadMedida nombre_unidad;
	private IntegerProperty id_producto, maximo, minimo, existentes, cod_unidad,cod_categoria,
	cantidad, posicion, venta;
	private DoubleProperty tamanio;
	private FloatProperty precio1, precio2, subtotal, costo;
	private ObservableList<Productos> elementos;
	private Conexion con;
	
	public Productos(){
		nombre = cientifico = estatus = new SimpleStringProperty();
		id_producto= maximo = minimo = existentes = cod_unidad = cod_categoria
		= cantidad = posicion = venta = new SimpleIntegerProperty();
		tamanio = new SimpleDoubleProperty();
		precio1 = precio2 = subtotal = costo = new SimpleFloatProperty();
		nombre_unidad = new UnidadMedida();
		nombre_categoria = new Categoria();
		con = Conexion.getInstancia();
	}

	public String getNombre() {
		return nombre.get();
	}

	public void setNombre(StringProperty nombre) {
		this.nombre = nombre;
	}

	public String getCientifico() {
		return cientifico.get();
	}

	public void setCientifico(StringProperty cientifico) {
		this.cientifico = cientifico;
	}

	public String getEstatus() {
		return estatus.get();
	}

	public void setEstatus(StringProperty estatus) {
		this.estatus = estatus;
	}

	public Categoria getNombre_categoria() {
		return nombre_categoria;
	}

	public void setNombre_categoria(Categoria nombre_categoria) {
		this.nombre_categoria = nombre_categoria;
	}

	public UnidadMedida getNombre_unidad() {
		return nombre_unidad;
	}

	public void setNombre_unidad(UnidadMedida nombre_unidad) {
		this.nombre_unidad = nombre_unidad;
	}

	public Integer getId_producto() {
		return id_producto.get();
	}

	public void setId_producto(IntegerProperty id_producto) {
		this.id_producto = id_producto;
	}
	
	public Float getPrecio1() {
		return precio1.get();
	}

	public void setPrecio1(FloatProperty precio1) {
		this.precio1 = precio1;
	}

	public Float getPrecio2() {
		return precio2.get();
	}

	public void setPrecio2(FloatProperty precio2) {
		this.precio2 = precio2;
	}

	public Integer getMaximo() {
		return maximo.get();
	}

	public void setMaximo(IntegerProperty maximo) {
		this.maximo = maximo;
	}

	public Integer getMinimo() {
		return minimo.get();
	}

	public void setMinimo(IntegerProperty minimo) {
		this.minimo = minimo;
	}

	public Integer getExistentes() {
		return existentes.get();
	}

	public void setExistentes(IntegerProperty cantidad) {
		this.existentes = cantidad;
	}

	public Integer getCod_unidad() {
		return cod_unidad.get();
	}

	public void setCod_unidad(IntegerProperty cod_unidad) {
		this.cod_unidad = cod_unidad;
	}

	public Integer getCod_categoria() {
		return cod_categoria.get();
	}

	public void setCod_categoria(IntegerProperty cod_categoria) {
		this.cod_categoria = cod_categoria;
	}

	public Double getTamanio() {
		return tamanio.get();
	}

	public void setTamanio(DoubleProperty tamanio) {
		this.tamanio = tamanio;
	}

	public ObservableList<Productos> getProductos(boolean estatus) throws SQLException {
		ResultSet rs = null;
		try{
			String sql = "";
			if(estatus)
				sql = "select _codigo, _nombre, _nombre_cientifico, _precio1, _precio2, _maximo, _minimo, _activo, _existentes, _cod_unidad,"
						+ " _cod_categoria, _tamanio, _categoria, _unidad from fn_tablaproductos() where _activo = 's';";
			else
				sql = "select _codigo, _nombre, _nombre_cientifico, _precio1, _precio2, _maximo, _minimo, _activo, _existentes, _cod_unidad,"
						+ " _cod_categoria, _tamanio, _categoria, _unidad from fn_tablaproductos() where _activo = 'n';";
			con.conectar();
			PreparedStatement comando = con.getConexion().prepareStatement(sql);
			rs = comando.executeQuery();
			elementos = FXCollections.observableArrayList();
			while(rs.next()){
				Productos product = new Productos();
				product.id_producto = new SimpleIntegerProperty(rs.getInt("_codigo"));
				product.nombre = new SimpleStringProperty(rs.getString("_nombre"));
				product.cientifico = new SimpleStringProperty(rs.getString("_nombre_cientifico"));
				product.tamanio = new SimpleDoubleProperty(rs.getDouble("_tamanio"));
				product.precio1 = new SimpleFloatProperty(rs.getFloat("_precio1"));
				product.precio2 = new SimpleFloatProperty(rs.getFloat("_precio2"));
				product.maximo = new SimpleIntegerProperty(rs.getInt("_maximo"));
				product.minimo = new SimpleIntegerProperty(rs.getInt("_minimo"));
				product.existentes = new SimpleIntegerProperty(rs.getInt("_existentes"));
				product.nombre_categoria.setNombre(new SimpleStringProperty(rs.getString("_categoria")));
				product.nombre_unidad.setNombre_unidad(new SimpleStringProperty(rs.getString("_unidad")));
				
				elementos.add(product);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		finally{
			rs.close();
			con.desconectar();
		}
		return elementos;
	}

	public boolean insertar() {
		try{
			String sql = "select fn_agregarproducto (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
			con.conectar();
			PreparedStatement comando = con.getConexion().prepareStatement(sql);
			comando.setString(1, this.getNombre());
			comando.setString(2, this.getCientifico());
			comando.setFloat(4, this.getPrecio2());
			comando.setFloat(3, this.getPrecio1());
			comando.setDouble(5, this.getTamanio());
			comando.setString(6, this.getNombre_unidad().getNombre_unidad());
			comando.setInt(7, this.getMaximo());
			comando.setInt(8, this.getMinimo());
			comando.setInt(9, this.getExistentes());
			comando.setString(10, this.getNombre_categoria().getNombre());
			
			comando.execute();
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		finally{
			con.desconectar();
			
		}
	}

	public boolean modificar() {
		try{
			String sql = "select fn_modificarproducto(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
			con.conectar();
			PreparedStatement comando = con.getConexion().prepareStatement(sql);
			comando.setInt(1, this.getId_producto());
			comando.setString(2, this.getNombre());
			comando.setString(3, this.getCientifico());
			comando.setFloat(4, this.getPrecio1());
			comando.setFloat(5, this.getPrecio2());
			comando.setDouble(6, this.getTamanio());
			comando.setInt(7, this.getMaximo());
			comando.setInt(8, this.getMinimo());
			comando.setInt(9, this.getExistentes());
			comando.setString(10, this.getNombre_unidad().toString());
			comando.setString(11, this.getNombre_categoria().toString());
			comando.execute();
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}

	public boolean eliminar() {
		try{
			String sql = "select fn_eliminarproducto(?)";
			con.conectar();
			PreparedStatement comando = con.getConexion().prepareStatement(sql);
			comando.setInt(1, this.getId_producto());
			comando.execute();
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}

	public boolean recuperarProducto() {
		try{
			String sql = "select fn_recuperarproducto(?)";
			con.conectar();
			PreparedStatement comando = con.getConexion().prepareStatement(sql);
			comando.setInt(1, this.getId_producto());
			comando.execute();
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
	
	public String toString(){
		return nombre.get();
	}

	public Integer getCantidad() {
		return cantidad.get();
	}

	public void setCantidad(IntegerProperty cantidad) {
		this.cantidad = cantidad;
	}

	public Float getSubtotal() {
		return subtotal.get();
	}

	public void setSubtotal(FloatProperty subtotal) {
		this.subtotal = subtotal;
	}

	public Integer getPosicion() {
		return posicion.get();
	}

	public void setPosicion(IntegerProperty posicion) {
		this.posicion = posicion;
	}

	public Integer getVenta() {
		return venta.get();
	}

	public void setVenta(IntegerProperty venta) {
		this.venta = venta;
	}

	public Float getCosto() {
		return costo.get();
	}

	public void setCosto(FloatProperty costo) {
		this.costo = costo;
	}
	
	
	
}
