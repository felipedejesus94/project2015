package modelo;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import sun.security.action.GetIntegerAction;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class UnidadMedida {
	private StringProperty nombre_unidad;
	private IntegerProperty id_unidad;
	private Conexion con;
	public ObservableList<UnidadMedida> elementos;
	
	public UnidadMedida(){
		nombre_unidad = new SimpleStringProperty();
		id_unidad = new SimpleIntegerProperty();
		con = Conexion.getInstancia();
	}

	public String getNombre_unidad() {
		return nombre_unidad.get();
	}

	public void setNombre_unidad(StringProperty nombre_unidad) {
		this.nombre_unidad = nombre_unidad;
	}

	public Integer getId_unidad() {
		return id_unidad.get();
	}

	public void setId_unidad(IntegerProperty id_unidad) {
		this.id_unidad = id_unidad;
	}
	
	public ObservableList<UnidadMedida> getUnidad(Boolean estatus) throws SQLException{
		ResultSet rs = null;
		try{
			String sql = "";
			if(estatus){
				sql = "select _nombre, cod_unidad from fn_tablaunidad() where _activo = 's'";
			}else{
				sql = "select _nombre, cod_unidad from fn_tablaunidad() where _activo = 'n'";
			}
			con.conectar();
			PreparedStatement comando = con.getConexion().prepareStatement(sql);
			rs = comando .executeQuery();
			elementos = FXCollections.observableArrayList();
			while(rs.next()){
				UnidadMedida ca = new UnidadMedida();
				ca.nombre_unidad.set(rs.getString("_nombre"));
				ca.id_unidad.set(rs.getInt("cod_unidad"));
				elementos.add(ca);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		finally{
			con.desconectar();
			rs.close();
		}
		return elementos;
	}
	
	public String toString(){
		return nombre_unidad.getValue();
	}

	public boolean ingresar() {
		try{
			String sql = "select fn_agregarunidad(?)";
			con.conectar();
			PreparedStatement comando = con.getConexion().prepareStatement(sql);
			comando.setString(1, this.getNombre_unidad());
			comando.execute();
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		finally{
			con.desconectar();
		}
	}

	public boolean modificar() {
		try{
			String sql = "select fn_modificarunidad(?,?)";
			con.conectar();
			PreparedStatement comando = con.getConexion().prepareStatement(sql);
			comando.setInt(1, this.getId_unidad());
			comando.setString(2, this.getNombre_unidad());
			comando.execute();
			System.out.println("entro al metodeichon");
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}

	public boolean eliminar() {
		try{
			String sql = "select fn_eliminarunidad(?)";
			con.conectar();
			PreparedStatement comando = con.getConexion().prepareStatement(sql);
			comando.setInt(1, this.getId_unidad());
			System.out.println(this.getId_unidad()+" en el metodo eliminar");
			comando.execute();
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
	public boolean recuperar() {
		try{
			String sql = "select fn_recuperarunidad(?)";
			con.conectar();
			PreparedStatement comando = con.getConexion().prepareStatement(sql);
			comando.setInt(1, this.getId_unidad());
			System.out.println(this.getId_unidad()+" en el metodo eliminar");
			comando.execute();
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
}
