package modelo;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.sun.org.apache.bcel.internal.generic.FCONST;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Categoria {
	private StringProperty nombre;
	private IntegerProperty id_categoria;
	private Conexion con;
	private ObservableList<Categoria> elementos;
	
	public Categoria(){
		con = Conexion.getInstancia();
		nombre = new SimpleStringProperty();
		id_categoria = new SimpleIntegerProperty();
	}

	public String getNombre() {
		return nombre.get();
	}

	public void setNombre(StringProperty nombre) {
		this.nombre = nombre;
	}

	public Integer getId_categoria() {
		return id_categoria.get();
	}

	public void setId_categoria(IntegerProperty id_categoria) {
		this.id_categoria = id_categoria;
	}
	
	public ObservableList<Categoria> getCategoria(Boolean estatus) throws SQLException{
		ResultSet rs = null;
		try{
			String sql = "";
			if(estatus){
				sql = "select _nombre, cod_categoria from fn_tablacategoria() where _activo = 's';";
			}else{
				sql = "select _nombre, cod_categoria from fn_tablacategoria() where _activo = 'n';";
			}
			con.conectar();
			PreparedStatement comando = con.getConexion().prepareStatement(sql);
			rs = comando .executeQuery();
			elementos = FXCollections.observableArrayList();
			while(rs.next()){
				Categoria ca = new Categoria();
				ca.nombre.set(rs.getString("_nombre"));
				ca.id_categoria.set(rs.getInt("cod_categoria"));
				elementos.add(ca);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		finally{
			con.desconectar();
			rs.close();
		}
		return elementos;
	}
	
	public boolean ingresar(){
		try{
			String sql = "select fn_agregarcategoria(?)";
			con.conectar();
			PreparedStatement comando = con.getConexion().prepareStatement(sql);
			comando.setString(1, this.getNombre());
			comando.execute();
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		finally{
			con.desconectar();
		}
	}
	
	public boolean modificar(){
		try{
			String sql = "select fn_modificarcategoria(?,?)";
			con.conectar();
			PreparedStatement comando = con.getConexion().prepareStatement(sql);
			comando.setInt(1, this.getId_categoria());
			comando.setString(2, this.getNombre());
			comando.execute();
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean eliminar(){
		try{
			String sql = "select fn_eliminarcategoria(?)";
			con.conectar();
			PreparedStatement comando = con.getConexion().prepareStatement(sql);
			comando.setInt(1, this.getId_categoria());
			comando.execute();
			System.out.println(this.getId_categoria());
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean recuperar(){
		try{
			String sql = "select fn_recuperarcategoria(?)";
			con.conectar();
			PreparedStatement comando = con.getConexion().prepareStatement(sql);
			comando.setInt(1, this.getId_categoria());
			comando.execute();
			System.out.println(this.getId_categoria());
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
	
	public String toString(){
		return nombre.getValue();
	}
}
