package modelo;

import javafx.beans.property.FloatProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class AdminVenta {
	private StringProperty nombre, fecha, telefono, correo;
	private IntegerProperty numero, codigo;
	private FloatProperty monto;
	
	public AdminVenta(){
		nombre = fecha = telefono = correo = new SimpleStringProperty();
		numero = codigo = new SimpleIntegerProperty();
		monto = new SimpleFloatProperty();
	}

	public String getNombre() {
		return nombre.get();
	}

	public void setNombre(StringProperty nombre) {
		this.nombre = nombre;
	}

	public String getFecha() {
		return fecha.get();
	}

	public void setFecha(StringProperty fecha) {
		this.fecha = fecha;
	}

	public String getTelefono() {
		return telefono.get();
	}

	public void setTelefono(StringProperty telefono) {
		this.telefono = telefono;
	}

	public String getCorreo() {
		return correo.get();
	}

	public void setCorreo(StringProperty correo) {
		this.correo = correo;
	}

	public Integer getNumero() {
		return numero.get();
	}

	public void setNumero(IntegerProperty numero) {
		this.numero = numero;
	}

	public Integer getCodigo() {
		return codigo.get();
	}

	public void setCodigo(IntegerProperty codigo) {
		this.codigo = codigo;
	}

	public Float getMonto() {
		return monto.get();
	}

	public void setMonto(FloatProperty monto) {
		this.monto = monto;
	}
	
	
}
