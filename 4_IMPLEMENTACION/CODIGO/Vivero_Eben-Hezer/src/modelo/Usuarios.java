package modelo;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Usuarios {
	private StringProperty nombre, aPaterno, aMaterno, contrasenia, telefono, correo,
	municipio, colonia, direccion;
	private String puesto, estado;
	private IntegerProperty idUsuario;
	public ObservableList<Usuarios> elementos;
	public Conexion con;
	
	public Usuarios(){
		nombre = aPaterno = aMaterno = contrasenia = telefono = correo = municipio
		= colonia = direccion = new SimpleStringProperty();
		idUsuario = new SimpleIntegerProperty();
		puesto = estado = new String();
		con = Conexion.getInstancia();
	}
	
	public String getNombre() {
		return nombre.get();
	}
	public void setNombre(StringProperty nombre) {
		this.nombre = nombre;
	}
	public String getaPaterno() {
		return aPaterno.get();
	}
	public void setaPaterno(StringProperty aPaterno) {
		this.aPaterno = aPaterno;
	}
	public String getaMaterno() {
		return aMaterno.get();
	}
	public void setaMaterno(StringProperty aMaterno) {
		this.aMaterno = aMaterno;
	}
	public String getPuesto() {
		return puesto;
	}
	public void setPuesto(String puesto) {
		this.puesto = puesto;
	}
	public String getContrasenia() {
		return contrasenia.get();
	}
	public void setContrasenia(StringProperty contrasenia) {
		this.contrasenia = contrasenia;
	}
	public String getTelefono() {
		return telefono.get();
	}
	public void setTelefono(StringProperty telefono) {
		this.telefono = telefono;
	}
	public String getCorreo() {
		return correo.get();
	}
	public void setCorreo(StringProperty correo) {
		this.correo = correo;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getMunicipio() {
		return municipio.get();
	}
	public void setMunicipio(StringProperty municipio) {
		this.municipio = municipio;
	}
	public String getColonia() {
		return colonia.get();
	}
	public void setColonia(StringProperty colonia) {
		this.colonia = colonia;
	}
	public String getDireccion() {
		return direccion.get();
	}
	public void setDireccion(StringProperty direccion) {
		this.direccion = direccion;
	}
	public Integer getIdUsuario() {
		return idUsuario.get();
	}
	public void setIdUsuario(IntegerProperty idUsuario) {
		this.idUsuario = idUsuario;
	}
	
	public boolean insertar(){
		try{
			String sql = "select fn_agregarusuario(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			con.conectar();
			PreparedStatement comando = con.getConexion().prepareStatement(sql);
			comando.setString(1, this.getNombre());
			comando.setString(2, this.getaPaterno());
			comando.setString(3, this.getaMaterno());
			comando.setString(4, this.getPuesto());
			comando.setString(5, this.getContrasenia());
			comando.setString(6, this.getTelefono());
			comando.setString(7, this.getCorreo());
			comando.setString(8, this.getEstado());
			comando.setString(9, this.getMunicipio());
			comando.setString(10, this.getColonia());
			comando.setString(11, this.getDireccion());
			comando.execute();
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}finally{
			con.desconectar();
		}
	}
	public boolean modificar(){
		try{
			String sql = "select fn_modificarusuario(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			con.conectar();
			PreparedStatement comando = con.getConexion().prepareStatement(sql);
			comando.setInt(1, this.getIdUsuario());
			comando.setString(2, this.getNombre());
			comando.setString(3, this.getaPaterno());
			comando.setString(4, this.getaMaterno());
			comando.setString(5, this.getPuesto());
			comando.setString(6, this.getContrasenia());
			comando.setString(7, this.getTelefono());
			comando.setString(8, this.getCorreo());
			comando.setString(9, this.getEstado());
			comando.setString(10, this.getMunicipio());
			comando.setString(11, this.getColonia());
			comando.setString(12, this.getDireccion());
			
			comando.execute();
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}finally{
			con.desconectar();
		}
	}
	
	public boolean eliminar(){
		try{
			String sql = "select fn_eliminarusuario(?)";
			con.conectar();
			PreparedStatement comando = con.getConexion().prepareStatement(sql);
			comando.setInt(1, this.getIdUsuario());
			comando.execute();
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean recuperar(){
		try{
			String sql = "select fn_recuperarusuario(?)";
			con.conectar();
			PreparedStatement comando = con.getConexion().prepareStatement(sql);
			comando.setInt(1, this.getIdUsuario());
			comando.execute();
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
	
	public ObservableList<Usuarios> getUsuarios(Boolean estatus) throws SQLException{
		ResultSet rs = null;
		try{
			String sql = " ";
			if(estatus){
				sql = "select _codigo, _nombre,_apaterno, _amaterno, _puesto, _contrasenia, _telefono, _correo, _estado, _municipio, _colonia, _direccion from fn_tablaUsuarios() where _activo = 's';";
			}else{
				sql = "select _codigo, _nombre,_apaterno, _amaterno, _puesto, _contrasenia, _telefono, _correo, _estado, _municipio, _colonia, _direccion from fn_tablaUsuarios() where _activo = 'n';";
			}
			con.conectar();
			PreparedStatement comando = con.getConexion().prepareStatement(sql);
			rs = comando.executeQuery();
			elementos = FXCollections.observableArrayList();
			while(rs.next()){
				Usuarios user = new Usuarios();
				user.idUsuario = new SimpleIntegerProperty(rs.getInt("_codigo"));
				user.nombre = new SimpleStringProperty(rs.getString("_nombre"));
				user.aPaterno = new SimpleStringProperty(rs.getString("_apaterno"));
				user.aMaterno = new SimpleStringProperty(rs.getString("_amaterno"));
				user.puesto = new String(rs.getString("_puesto"));
				user.contrasenia = new SimpleStringProperty(rs.getString("_contrasenia"));
				user.telefono = new SimpleStringProperty(rs.getString("_telefono"));
				user.correo = new SimpleStringProperty(rs.getString("_correo"));
				user.estado = new String(rs.getString("_estado"));
				user.municipio = new SimpleStringProperty(rs.getString("_municipio"));
				user.colonia = new SimpleStringProperty(rs.getString("_colonia"));
				user.direccion = new SimpleStringProperty(rs.getString("_direccion"));
				elementos.add(user);
			}
		}catch(Exception e){
			e.printStackTrace();
			
		}
		finally{
			rs.close();
			con.desconectar();
		}
		return elementos;
	}
}
