package modelo;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Proveedores {
	private StringProperty telefono, correo, municipio, colonia, direccion, nombre, 
	apellido_paterno, apellido_materno, nombre_empresa, observaciones, nombre_contacto, apellidop, apellidom, tipo, observations;
	private String estado;
	private IntegerProperty id_proveedor, cod_proveedor, cod_empresa;
	private ObservableList<Proveedores> elementos;
	private Conexion con;
	
	public Proveedores(){
		telefono = correo = municipio = colonia = direccion = nombre = 
		apellido_paterno = apellido_materno = nombre_empresa = observaciones = nombre_contacto = apellidop = apellidom = new SimpleStringProperty();
		estado = new String();
		id_proveedor = cod_proveedor = cod_empresa = new SimpleIntegerProperty();
		con = Conexion.getInstancia();
	}
	
	
	public boolean insertar(){
		try{
			String sql = "select * from fn_agregarproveedor(?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
			con.conectar();
			PreparedStatement comando = con.getConexion().prepareStatement(sql);
			comando.setString(1, this.getTelefono());
			comando.setString(2, this.getCorreo());
			comando.setString(3, this.getEstado());
			comando.setString(4, this.getMunicipio());
			comando.setString(5, this.getColonia());
			comando.setString(6, this.getDireccion());
			comando.setString(7, this.getNombre());
			comando.setString(8, this.getApellido_paterno());
			comando.setString(9, this.getApellido_materno());
			comando.setString(10, this.getObservations());
			
			comando.execute();
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		finally{
			con.desconectar();
		}
	}
	
	public boolean insertarMoral() {
		try{
			String sql = "select from fn_agregarproveedorMoral(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
			con.conectar();
			PreparedStatement comando = con.getConexion().prepareStatement(sql);
			comando.setString(1, this.getTelefono());
			comando.setString(2, this.getCorreo());
			comando.setString(3, this.getEstado());
			comando.setString(4, this.getMunicipio());
			comando.setString(5, this.getColonia());
			comando.setString(6, this.getDireccion());
			comando.setString(7, this.getNombre_empresa());
			comando.setString(8, this.getNombre_contacto());
			comando.setString(9, this.getApellidop());
			comando.setString(10, this.getApellidom());
			comando.setString(11, this.getObservaciones());
			
			comando.execute();
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		finally{
			con.desconectar();
		}
	}
	
	public boolean modificar() {
		try{
			String sql = "select  from fn_modificarproveedmoral(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
			con.conectar();
			PreparedStatement comando = con.getConexion().prepareStatement(sql);
			comando.setInt(1, this.getId_proveedor());
			comando.setString(2, this.getTelefono());
			comando.setString(3, this.getCorreo());
			comando.setString(4, this.getEstado());
			comando.setString(5, this.getMunicipio());
			comando.setString(6, this.getColonia());
			comando.setString(7, this.getDireccion());
			comando.setString(8, this.getNombre_empresa());
			comando.setString(9, this.getObservaciones());
			comando.setString(10, this.getNombre_contacto());
			comando.setString(11, this.getApellidop());
			comando.setString(12, this.getApellidom());
			comando.setInt(13, this.getCod_empresa());
			comando.execute();
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean modificarFisico() {
		try{
			String sql = "select from fn_modificarproveedor(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
			con.conectar();
			PreparedStatement comando = con.getConexion().prepareStatement(sql);
			comando.setInt(1, this.getId_proveedor());
			comando.setString(2, this.getTelefono());
			comando.setString(3, this.getCorreo());
			comando.setString(4, this.getEstado());
			comando.setString(5, this.getMunicipio());
			comando.setString(6, this.getColonia());
			comando.setString(7, this.getDireccion());
			comando.setString(8, this.getNombre());
			comando.setString(9, this.getApellidop());
			comando.setString(10, this.getApellidom());
			comando.setString(11, this.getObservations());
			comando.setInt(12, this.getCod_proveedor());
			comando.execute();
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean eliminar() {
		try{
			String sql = "select fn_eliminarproveedor (?)";
			con.conectar();
			PreparedStatement comando = con.getConexion().prepareStatement(sql);
			comando.setInt(1, this.getId_proveedor());
			comando.execute();
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		finally{
			con.desconectar();
		}
	}
	
	
	public String getTelefono() {
		return telefono.get();
	}

	public void setTelefono(StringProperty telefono) {
		this.telefono = telefono;
	}

	public String getCorreo() {
		return correo.get();
	}

	public void setCorreo(StringProperty correo) {
		this.correo = correo;
	}

	

	public String getEstado() {
		return estado;
	}


	public void setEstado(String estado) {
		this.estado = estado;
	}


	public String getMunicipio() {
		return municipio.get();
	}

	public void setMunicipio(StringProperty municipio) {
		this.municipio = municipio;
	}

	public String getColonia() {
		return colonia.get();
	}

	public void setColonia(StringProperty colonia) {
		this.colonia = colonia;
	}

	public String getDireccion() {
		return direccion.get();
	}

	public void setDireccion(StringProperty direccion) {
		this.direccion = direccion;
	}

	public String getNombre() {
		return nombre.get();
	}

	public void setNombre(StringProperty nombre) {
		this.nombre = nombre;
	}

	public String getApellido_paterno() {
		return apellido_paterno.get();
	}

	public void setApellido_paterno(StringProperty apellido_paterno) {
		this.apellido_paterno = apellido_paterno;
	}

	public String getApellido_materno() {
		return apellido_materno.get();
	}

	public void setApellido_materno(StringProperty apellido_materno) {
		this.apellido_materno = apellido_materno;
	}

	public String getNombre_empresa() {
		return nombre_empresa.get();
	}

	public void setNombre_empresa(StringProperty nombre_empresa) {
		this.nombre_empresa = nombre_empresa;
	}

	public String getObservaciones() {
		return observaciones.get();
	}

	public void setObservaciones(StringProperty observaciones) {
		this.observaciones = observaciones;
	}

	public String getNombre_contacto() {
		return nombre_contacto.get();
	}

	public void setNombre_contacto(StringProperty nombre_contacto) {
		this.nombre_contacto = nombre_contacto;
	}

	public String getApellidop() {
		return apellidop.get();
	}

	public void setApellidop(StringProperty apellidop) {
		this.apellidop = apellidop;
	}

	public String getApellidom() {
		return apellidom.get();
	}

	public void setApellidom(StringProperty apellidom) {
		this.apellidom = apellidom;
	}

	public Integer getId_proveedor() {
		return id_proveedor.get();
	}

	public void setId_proveedor(IntegerProperty id_proveedor) {
		this.id_proveedor = id_proveedor;
	}

	
	public Integer getCod_proveedor() {
		return cod_proveedor.get();
	}


	public void setCod_proveedor(IntegerProperty cod_proveedor) {
		this.cod_proveedor = cod_proveedor;
	}


	public Integer getCod_empresa() {
		return cod_empresa.get();
	}


	public void setCod_empresa(IntegerProperty cod_empresa) {
		this.cod_empresa = cod_empresa;
	}

	
	public String getObservations() {
		return observations.get();
	}


	public void setObservations(StringProperty observations) {
		this.observations = observations;
	}


	public ObservableList<Proveedores> getProveedor(Boolean estatus) throws SQLException {
		ResultSet rs = null;
		try{
			String sql = "";
			if(estatus){
				sql = "select _codigo, _empresa, _nombre, _telefono, _correo, _estado, _municipio, _colonia, _direccion, _apellidop, _apellidom, _codigo_for, _observaciones, _tipo from tipos_de_proveedores() where _activo = 's';";
			}else{
				sql = "select _codigo, _empresa, _nombre, _telefono, _correo, _estado, _municipio, _colonia, _direccion, _apellidop, _apellidom, _codigo_for, _observaciones, _tipo from tipos_de_proveedores() where _activo = 'n';";
			}
			con.conectar();
			PreparedStatement comando = con.getConexion().prepareStatement(sql);
			rs = comando.executeQuery();
			elementos = FXCollections.observableArrayList();
			while(rs.next()){
				Proveedores client = new Proveedores();
				client.id_proveedor = new SimpleIntegerProperty(rs.getInt("_codigo"));
				client.nombre_empresa = new SimpleStringProperty(rs.getString("_empresa"));
				client.nombre = new SimpleStringProperty(rs.getString("_nombre"));
				client.nombre_contacto = new SimpleStringProperty(rs.getString("_nombre"));
				client.telefono = new SimpleStringProperty(rs.getString("_telefono"));
				client.correo = new SimpleStringProperty(rs.getString("_correo"));
				client.estado = new String(rs.getString("_estado"));
				client.municipio = new SimpleStringProperty(rs.getString("_municipio"));
				client.colonia = new SimpleStringProperty(rs.getString("_colonia"));
				client.direccion = new SimpleStringProperty(rs.getString("_direccion"));
				client.apellido_paterno = new SimpleStringProperty(rs.getString("_apellidop"));
				client.apellidop = new SimpleStringProperty(rs.getString("_apellidop"));
				client.apellido_materno = new SimpleStringProperty(rs.getString("_apellidom"));
				client.apellidom = new SimpleStringProperty(rs.getString("_apellidom"));
				client.cod_proveedor = new SimpleIntegerProperty(rs.getInt("_codigo_for"));
				client.cod_empresa = new SimpleIntegerProperty(rs.getInt("_codigo_for"));
				client.observaciones = new SimpleStringProperty(rs.getString("_observaciones"));
				client.tipo = new SimpleStringProperty(rs.getString("_tipo"));
			
				elementos.add(client);
			}
		}catch(Exception e){
			e.printStackTrace();
			
		}
		finally{
			rs.close();
			con.desconectar();
		}
		return elementos;
		
	}

	public String toString(){
		return nombre.get();
		
	}
	public String getTipo() {
		return tipo.get();
	}


	public void setTipo(StringProperty tipo) {
		this.tipo = tipo;
	}
}
