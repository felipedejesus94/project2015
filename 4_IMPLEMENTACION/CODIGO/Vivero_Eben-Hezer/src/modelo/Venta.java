package modelo;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.sun.corba.se.pept.transport.Connection;

import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Venta {
	private int posicionProductoEnTabla;
	private Integer cantidad, detalle_id, venta_id, existentes, cliente_id, empleado_id;
	private float total, precio;
	private Productos productos;
	private ObservableList<Productos> listaDetalles;
	private Conexion con;
	private Detalle d;
	private java.sql.Connection miconexion;
	private PreparedStatement comando;
	//private int posicionProductoEnTabla;
	
	public Venta(){
		cantidad = detalle_id = venta_id= cliente_id=empleado_id = 0;
		productos = new Productos();
		listaDetalles = FXCollections.observableArrayList();
		con = Conexion.getInstancia();
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public Integer getDetalle_id() {
		return detalle_id;
	}

	public void setDetalle_id(Integer detalle_id) {
		this.detalle_id = detalle_id;
	}

	public Integer getVenta_id() {
		return venta_id;
	}

	public void setVenta_id(Integer venta_id) {
		this.venta_id = venta_id;
	}

	public Productos getProductos() {
		return productos;
	}

	public void setProductos(Productos productos) {
		this.productos = productos;
	}
	
	public Integer getExistentes() {
		return existentes;
	}

	public void setExistentes(Integer existentes) {
		this.existentes = existentes;
	}
	
	
	public float getPrecio() {
		return precio;
	}

	public void setPrecio(float precio) {
		this.precio = precio;
	}

	public int getPosicionProductoEnTabla() {
		return posicionProductoEnTabla;
	}

	public void setPosicionProductoEnTabla(int posicionProductoEnTabla) {
		this.posicionProductoEnTabla = posicionProductoEnTabla;
	}
	
	public boolean agregarDetalle(){
		boolean existe = false;
		try{
			if(listaDetalles.isEmpty() || existe == false){
				Productos producto = new Productos();
				producto.setVenta(new SimpleIntegerProperty(venta_id));
				//producto.setUsuario(new SimpleIntegerProperty(empleado_id));
				//System.out.println(empleado_id+ "id del empleado");
				//producto.setCliente(new SimpleIntegerProperty(empleado_id));
				//System.out.println(cliente_id + "id del cliente");
				producto.setId_producto(new SimpleIntegerProperty(productos.getId_producto()));
				producto.setCantidad(new SimpleIntegerProperty(cantidad));
				producto.setPrecio2(new SimpleFloatProperty(productos.getPrecio2()));
				producto.setPrecio1(new SimpleFloatProperty(productos.getPrecio1()));
				producto.setSubtotal(new SimpleFloatProperty(cantidad * productos.getPrecio2()));
				producto.setNombre(new SimpleStringProperty(productos.getNombre()));
				producto.setExistentes(new SimpleIntegerProperty(productos.getExistentes()));
				System.out.println(posicionProductoEnTabla+" simple en clase venta");
				//System.out.println(getPosicionProductoEnTabla()+"con get clase venta");
				producto.setPosicion(new SimpleIntegerProperty(posicionProductoEnTabla));
				System.out.println(listaDetalles.size()+"talla");
				listaDetalles.add(producto);
			}
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean editarDetalle(){
		boolean existe = false;
		try{
			if(listaDetalles.isEmpty() || existe == false){
				Productos producto = new Productos();
				producto.setVenta(new SimpleIntegerProperty(venta_id));
				//producto.setUsuario(new SimpleIntegerProperty(empleado_id));
				//System.out.println(empleado_id+ "id del empleado");
				//producto.setCliente(new SimpleIntegerProperty(empleado_id));
				//System.out.println(cliente_id + "id del cliente");
				producto.setId_producto(new SimpleIntegerProperty(productos.getId_producto()));
				producto.setCantidad(new SimpleIntegerProperty(cantidad));
				producto.setPrecio2(new SimpleFloatProperty(productos.getPrecio2()));
				producto.setPrecio1(new SimpleFloatProperty(productos.getPrecio1()));
				producto.setSubtotal(new SimpleFloatProperty(cantidad * productos.getPrecio2()));
				producto.setNombre(new SimpleStringProperty(productos.getNombre()));
				producto.setExistentes(new SimpleIntegerProperty(productos.getExistentes()));
				System.out.println(posicionProductoEnTabla+" simple en clase venta");
				//System.out.println(getPosicionProductoEnTabla()+"con get clase venta");
				producto.setPosicion(new SimpleIntegerProperty(posicionProductoEnTabla));
				System.out.println(listaDetalles.size()+"talla");
				listaDetalles.set(posicionProductoEnTabla, producto);
			}else{
				System.out.println("no esta vacia");
			}
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean eliminarDetalle(){
		boolean existe = false;
		try{
			if(listaDetalles.isEmpty() || existe == false){
				listaDetalles.remove(this.getPosicionProductoEnTabla());
				System.out.println(this.getPosicionProductoEnTabla()+"este recibo y elimino");
			}
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
	
	public float getTotal() {
		total = 0;
		for(Productos d: listaDetalles){
			total += d.getSubtotal();
		}
		return total;
	}
	
	public ObservableList<Productos> obtenerDetalle(){
		return listaDetalles;
	}
	
	public void setListaDetalles(ObservableList<Productos> listaDetalles) {
		this.listaDetalles = listaDetalles;
	}

	public boolean guardar(){
		try {

			String sql ="select fn_insertarVenta(?,?)";
			String sql2;
			con.conectar();	
			con.getConexion().setAutoCommit(false);
			PreparedStatement comando1 = con.getConexion().prepareStatement(sql);
			comando1.setInt(1, this.getEmpleado_id());
			comando1.setInt(2, this.getCliente_id());
			System.out.println(this.getEmpleado_id()+"en la insercion");
			PreparedStatement comando2;
			comando1.execute();
			//Insertar detalle
			for(Productos d: listaDetalles){
				sql="select fn_insertarDetalle(?, ?, ?, ?)";
				comando2 = con.getConexion().prepareStatement(sql);
				comando2.setFloat(1, d.getPrecio2());
				comando2.setInt(2, d.getCantidad());
				comando2.setInt(3, d.getId_producto());
				comando2.setInt(4, d.getVenta());
				comando2.execute();
			}
			con.getConexion().commit();
			con.getConexion().setAutoCommit(true);
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		}
		finally{
			con.desconectar();
		}
	}
	
	public boolean Existe(boolean bandera){
		con = Conexion.getInstancia();
		bandera = false;
		try{
			String sql = "select max(id_venta)as maximo from venta;";
			
			con.conectar();
			miconexion = con.getConexion();
			comando = con.getConexion().prepareStatement(sql);
			
			ResultSet rs = comando.executeQuery();
			while(rs.next()){
				this.setVenta_id(rs.getInt("maximo")+1);
				bandera = true;
			}
		}catch(Exception e){
			System.out.println("********************ERROR");
			e.printStackTrace();
		}
		finally{
			con.desconectar();
		}
		return bandera;
	}
	
	public boolean agragar(){
		try {

			String sql ="select fn_insertarVentaNormal(?)";
			String sql2;
			con.conectar();	
			con.getConexion().setAutoCommit(false);
			PreparedStatement comando1 = con.getConexion().prepareStatement(sql);
			comando1.setInt(1, this.getEmpleado_id());
			System.out.println(this.getEmpleado_id()+"no cliente");
			PreparedStatement comando2;
			comando1.execute();
			//Insertar detalle
			for(Productos d: listaDetalles){
				sql="select fn_insertarDetalle(?, ?, ?, ?)";
				comando2 = con.getConexion().prepareStatement(sql);
				comando2.setFloat(1, d.getPrecio2());
				comando2.setInt(2, d.getCantidad());
				comando2.setInt(3, d.getId_producto());
				comando2.setInt(4, d.getVenta());
				comando2.execute();
			}
			con.getConexion().commit();
			con.getConexion().setAutoCommit(true);
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		}
		finally{
			con.desconectar();
		}
	}


	public Integer getCliente_id() {
		return cliente_id;
	}

	public Integer setCliente_id(Integer cliente_id) {
		this.cliente_id = cliente_id;
		return cliente_id;
	}

	public Integer getEmpleado_id() {
		return empleado_id;
	}

	public void setEmpleado_id(Integer empleado_id) {
		this.empleado_id = empleado_id;
	}

	public Detalle getD() {
		return d;
	}

	public void setD(Detalle d) {
		this.d = d;
	}

	
}
