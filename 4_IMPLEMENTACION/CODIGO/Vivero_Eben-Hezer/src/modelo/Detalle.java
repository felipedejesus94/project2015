package modelo;

import java.sql.PreparedStatement;

import javafx.beans.property.FloatProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Detalle {
	private IntegerProperty cantidad, producto_id, existentes, id_Detalle, venta, usuario, posicion, cliente;
	private FloatProperty subtotal, precio;
	private StringProperty producto;
	private Conexion con;
	
	public Detalle(){
		cantidad= producto_id = existentes= id_Detalle= venta = usuario = posicion = cliente = new SimpleIntegerProperty();
		subtotal = precio = new SimpleFloatProperty();
		producto = new SimpleStringProperty();
		con = Conexion.getInstancia();
	}

	public Integer getCantidad() {
		return cantidad.get();
	}

	public void setCantidad(IntegerProperty cantidad) {
		this.cantidad = cantidad;
	}

	public Integer getProducto_id() {
		return producto_id.get();
	}

	public void setProducto_id(IntegerProperty producto_id) {
		this.producto_id = producto_id;
	}

	public Float getSubtotal() {
		return subtotal.get();
	}

	public void setSubtotal(FloatProperty subtotal) {
		this.subtotal = subtotal;
	}

	public Float getPrecio() {
		return precio.get();
	}

	public void setPrecio(FloatProperty precio) {
		this.precio = precio;
	}

	public String getProducto() {
		return producto.get();
	}

	public void setProducto(StringProperty producto) {
		this.producto = producto;
	}

	public Integer getExistentes() {
		return existentes.get();
	}

	public void setExistentes(IntegerProperty existentes) {
		this.existentes = existentes;
	}

	public Integer getId_Detalle() {
		return id_Detalle.get();
	}

	public void setId_Detalle(IntegerProperty id_Detalle) {
		this.id_Detalle = id_Detalle;
	}
	

	public Integer getVenta() {
		return venta.get();
	}

	public void setVenta(IntegerProperty venta) {
		this.venta = venta;
	}

	public Integer getUsuario() {
		return usuario.get();
	}

	public void setUsuario(IntegerProperty usuario) {
		this.usuario = usuario;
	}

	public Integer getPosicion() {
		return posicion.get();
	}

	public void setPosicion(IntegerProperty posicion) {
		this.posicion = posicion;
	}

	public boolean insertar(boolean existe) {
		try{
			String sql = "select fn_insertarDetalle(?, ?, ?, ?)";
			con.conectar();
			PreparedStatement comando = con.getConexion().prepareStatement(sql);
			comando.setFloat(1, this.getPrecio());
			comando.setInt(2, this.getCantidad());
			comando.setInt(3, this.getProducto_id());
			comando.setInt(4, this.getVenta());
			comando.execute();
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		
	}

	public Integer getCliente() {
		return cliente.get();
	}

	public void setCliente(IntegerProperty cliente) {
		this.cliente = cliente;
	}
	
	
}
