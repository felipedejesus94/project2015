package modelo;

import java.sql.Connection;
import java.sql.DriverManager;

import controlador.Errores;

public class Conexion {

	 
	private String bd; 
	private String usuario; 
	private String contrasenia;
	private String servidor;
	private String puerto;
	private String direccion;
	
	private static Conexion instancia;
	private Connection conn;
	private Errores ce;
	
	private Conexion(){
		 bd ="eben_hezer";
		 usuario = "postgres";
		 contrasenia = "coldplay";
		 servidor ="jdbc:postgresql://";
		 puerto = "5432";
		 direccion = "127.0.0.1";
		 conn = null;
		 ce = new Errores();
	}
	private Conexion(String bd, String usuario, String contrasenia, String servidor, String puerto, String direccion){
		this.bd = bd;
		this.usuario = usuario;
		this.contrasenia = contrasenia;
		this.servidor = servidor;
		this.puerto = puerto;
		this.direccion = direccion;
	}
	
	public static Conexion getInstancia(){
		if(instancia == null){
			instancia = new Conexion();
		}
		return instancia;
	}
	
	public String conectar(){
		try{
			Class.forName("org.postgresql.Driver");
			puerto = "5432";
			direccion = "localhost";
			servidor ="jdbc:postgresql://";
			conn = DriverManager.getConnection(servidor + direccion +":"+ puerto +"/"+bd, usuario, contrasenia);
			System.out.println("se ha establecido la conexion");
			return "conexion exitosa";
		} catch (Exception e){
			//e.printStackTrace();
			ce.printLog(e.getMessage(), this.getClass().toString());
			return "No se establecio la conexion. Consulte a su administrador";
		}
	}
	
	public String desconectar(){
		try{
			conn.close();
			System.out.println("se ha desconectado del servidor");
			return "se ha desconectado del servidor";
		}catch(Exception e){
			e.printStackTrace();
			return "La conexion esta siendo ocupada. No se puede desconectar";
		}
	}
	
	public Connection getConexion(){
		return conn;
		
	}
	public String getbd() {
		return bd;
	}
	public void setbd(String bd) {
		this.bd = bd;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getContrasenia() {
		return contrasenia;
	}
	public void setContrasenia(String contrasenia) {
		this.contrasenia = contrasenia;
	}
	public String getPuerto() {
		return puerto;
	}
	public void setPuerto(String puerto) {
		this.puerto = puerto;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
}


