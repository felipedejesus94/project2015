package modelo;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Compra {
	private int posicionProductoEnLaTabla;
	private Integer cantidad, detalle_id, compra_id, existentes, proveedor_id, empleado_id, maximo;
	private float total, precio1, precio2, costo;
	private Date fecha;
	private Productos productos;
	private ObservableList<Productos> listaDetalles;
	private Conexion con;
	private Detalle d;
	private Connection miConexion;
	private PreparedStatement comando;
	
	public Compra(){
		cantidad = detalle_id = compra_id = existentes = proveedor_id = empleado_id =maximo= 0;
		productos = new Productos();
		listaDetalles = FXCollections.observableArrayList();
		con = Conexion.getInstancia();
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public Integer getDetalle_id() {
		return detalle_id;
	}

	public void setDetalle_id(Integer detalle_id) {
		this.detalle_id = detalle_id;
	}

	public Integer getCompra_id() {
		return compra_id;
	}

	public void setCompra_id(Integer compra_id) {
		this.compra_id = compra_id;
	}

	public Integer getExistentes() {
		return existentes;
	}

	public void setExistentes(Integer existentes) {
		this.existentes = existentes;
	}

	public Integer getProveedor_id() {
		return proveedor_id;
	}

	public void setProveedor_id(Integer proveedor_id) {
		this.proveedor_id = proveedor_id;
	}

	public Integer getEmpleado_id() {
		return empleado_id;
	}

	public void setEmpleado_id(Integer empleado_id) {
		this.empleado_id = empleado_id;
	}

	public float getPrecio1() {
		return precio1;
	}

	public void setPrecio1(float precio1) {
		this.precio1 = precio1;
	}

	public float getPrecio2() {
		return precio2;
	}

	public void setPrecio2(float precio2) {
		this.precio2 = precio2;
	}

	public float getCosto() {
		return costo;
	}

	public void setCosto(float costo) {
		this.costo = costo;
	}
	

	public Integer getMaximo() {
		return maximo;
	}

	public void setMaximo(Integer maximo) {
		this.maximo = maximo;
	}
	

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public float getTotal() {
		total = 0;
		for(Productos d: listaDetalles){
			total += d.getSubtotal();
		}
		return total;
	}

	public void setTotal(float total) {
		this.total = total;
	}
	
	public Productos getProductos() {
		return productos;
	}

	public void setProductos(Productos productos) {
		this.productos = productos;
	}

	public int getPosicionProductoEnLaTabla() {
		return posicionProductoEnLaTabla;
	}

	public void setPosicionProductoEnLaTabla(int posicionProductoEnLaTabla) {
		this.posicionProductoEnLaTabla = posicionProductoEnLaTabla;
	}

	public ObservableList<Productos> getListaDetalles() {
		return listaDetalles;
	}

	public void setListaDetalles(ObservableList<Productos> listaDetalles) {
		this.listaDetalles = listaDetalles;
	}
	public ObservableList<Productos> obtenerDetalle(){
		return listaDetalles;
	}
	public boolean agregarDetalle(){
		boolean existe = false;
		try{
			if(listaDetalles.isEmpty() || existe == false){
				Productos producto = new Productos();
				producto.setVenta(new SimpleIntegerProperty(compra_id));
				producto.setId_producto(new SimpleIntegerProperty(productos.getId_producto()));
				producto.setCantidad(new SimpleIntegerProperty(cantidad));
				producto.setCosto(new SimpleFloatProperty(this.getCosto()));
				producto.setPrecio2(new SimpleFloatProperty(this.getPrecio2()));
				producto.setPrecio1(new SimpleFloatProperty(this.getPrecio1()));
				producto.setSubtotal(new SimpleFloatProperty(cantidad * costo));
				producto.setMaximo(new SimpleIntegerProperty(maximo));
				producto.setNombre(new SimpleStringProperty(productos.getNombre()));
				producto.setExistentes(new SimpleIntegerProperty(productos.getExistentes()));
				producto.setPosicion(new SimpleIntegerProperty(posicionProductoEnLaTabla));
				listaDetalles.add(producto);
			}
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean editarDetalle(){
		boolean existe = false;
		try{
			if(listaDetalles.isEmpty() || existe == false){
				Productos producto = new Productos();
				producto.setVenta(new SimpleIntegerProperty(compra_id));
				producto.setId_producto(new SimpleIntegerProperty(productos.getId_producto()));
				producto.setCantidad(new SimpleIntegerProperty(cantidad));
				producto.setSubtotal(new SimpleFloatProperty(cantidad * costo));
				producto.setNombre(new SimpleStringProperty(productos.getNombre()));
				producto.setExistentes(new SimpleIntegerProperty(productos.getExistentes()));
				producto.setCosto(new SimpleFloatProperty(this.getCosto()));
				producto.setPrecio1(new SimpleFloatProperty(this.getPrecio1()));
				producto.setPrecio2(new SimpleFloatProperty(this.getPrecio2()));
				producto.setMaximo(new SimpleIntegerProperty(maximo));
				producto.setPosicion(new SimpleIntegerProperty(posicionProductoEnLaTabla));
				listaDetalles.set(posicionProductoEnLaTabla, producto);
			}else{
				System.out.println("no esta vacia");
			}
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean eliminarDetalle(){
		boolean existe = false;
		try{
			if(listaDetalles.isEmpty() || existe == false){
				listaDetalles.remove(this.getPosicionProductoEnLaTabla());
				System.out.println(posicionProductoEnLaTabla+" el se eliminara");
			}
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean guardar(){
		try {

			String sql ="select fn_insertarcompra(?,?,?)";
			String sql2;
			con.conectar();	
			con.getConexion().setAutoCommit(false);
			PreparedStatement comando1 = con.getConexion().prepareStatement(sql);
			comando1.setInt(1, this.getProveedor_id());
			comando1.setInt(2, this.getEmpleado_id());
			comando1.setDate(3, this.getFecha());
			System.out.println(this.getEmpleado_id()+"en la insercion");
			PreparedStatement comando2;
			comando1.execute();
			//Insertar detalle
			for(Productos d: listaDetalles){
				sql="select fn_agregardetalle(?, ?, ?, ?, ?, ?)";
				comando2 = con.getConexion().prepareStatement(sql);
				comando2.setFloat(1, d.getCosto());
				comando2.setInt(2, d.getCantidad());
				comando2.setFloat(3, d.getPrecio1());
				comando2.setFloat(4, d.getPrecio2());
				comando2.setInt(5, this.getCompra_id());
				comando2.setInt(6, d.getId_producto());
				comando2.execute();
			}
			con.getConexion().commit();
			con.getConexion().setAutoCommit(true);
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		}
		finally{
			con.desconectar();
		}
	}
	
	public boolean Existe(boolean bandera){
		con = Conexion.getInstancia();
		bandera = false;
		try{
			String sql = "select max(id_compra)as maximo from compra;";
			
			con.conectar();
			miConexion = con.getConexion();
			comando = con.getConexion().prepareStatement(sql);
			
			ResultSet rs = comando.executeQuery();
			while(rs.next()){
				this.setCompra_id(rs.getInt("maximo")+1);
				bandera = true;
			}
		}catch(Exception e){
			System.out.println("********************ERROR");
			e.printStackTrace();
		}
		finally{
			con.desconectar();
		}
		return bandera;
	}
	
}
